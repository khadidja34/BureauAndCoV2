<?php

namespace BureauAndCoStatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\Response;	
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;


class DefaultController extends Controller
{
    /**
     * @Route("/statistique", name="statistique")
     * @Template()
     
     */
   public function statAction()
{
    if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
    $series = array(
        array("name" => "Data Serie Name",    "data" => array(1,2,4,5,6,3,8))
    );

    $ob = new Highchart();
    $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
    $ob->title->text('Nombre de clients');
    $ob->xAxis->title(array('text'  => "Date d'arrivée"));
    $ob->yAxis->title(array('text'  => "nombre"));
    $ob->series($series);

    return $this->render('BureauAndCoStatBundle:Default:stat.html.twig', array(
        'chart' => $ob
    ));
  }

    else{
        return new Response("Accès interdit à cette ressource");
    }
}
}
