<?php

namespace BureauAndCo\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Prospect client
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Cette email est déja utilisé par une autre personne");
 * @UniqueEntity(fields="tel", message="Ce numéro de télephone est déja attribué à une autre personne.")
 * @UniqueEntity(fields="nom", message="Ce cLient est déja dans le système.")
 * 
 */
class Prospect extends Client
{
   
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(checkMX=true,message=" Veuillez saisir un Email valid SVP")
     * @Assert\NotBlank(message="Ce champs est obligatoire")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=15)
     * @Assert\NotBlank(message="Ce champs est obligatoire")
     * @Assert\Length(max=13, maxMessage="contenu trop long!.")
     */
    private $tel;
    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     * @Assert\NotBlank(message="Ce champs est obligatoire")
     */
    private $ville;
    /**
     * Set email
     *
     * @param string $email
     * @return Prospect
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Prospect
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }


    /**
     * Set ville
     *
     * @param string $ville
     * @return Prospect
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }
}
