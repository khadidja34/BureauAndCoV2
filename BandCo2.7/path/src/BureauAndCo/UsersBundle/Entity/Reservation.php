<?php

namespace BureauAndCo\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BureauAndCo\SiteBundle\Entity\Site;

/**
 * Reservation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BureauAndCo\UsersBundle\Entity\ReservationRepository")
 */
class Reservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BureauAndCo\UsersBundle\Entity\Client", inversedBy="reservation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomClient;

     /**
     * @ORM\ManyToOne(targetEntity="BureauAndCo\SiteBundle\Entity\Site",inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="Surface", type="string", length=255)
     */
    private $surface;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateReservation", type="date")
     */
    private $dateReservation;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmation", type="string", length=255)
     */
    private $confirmation;
    public function __construct()

       {
           $this->dateReservation = new \Datetime();
           $this->confirmation="non";

       }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomClient
     *
     * @param string $nomClient
     * @return Reservation
     */
    public function setNomClient(Client $nomClient)
    {
        $this->nomClient = $nomClient;

        return $this;
    }

    /**
     * Get nomClient
     *
     * @return string 
     */
    public function getNomClient()
    {
        return $this->nomClient;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Reservation
     */
    public function setSite(Site $site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set surface
     *
     * @param string $surface
     * @return Reservation
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return string 
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set dateReservation
     *
     * @param \DateTime $dateReservation
     * @return Reservation
     */
    public function setDateReservation($dateReservation)
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    /**
     * Get dateReservation
     *
     * @return \DateTime 
     */
    public function getDateReservation()
    {
        return $this->dateReservation;
    }

    /**
     * Set confirmation
     *
     * @param string $confirmation
     * @return Reservation
     */
    public function setConfirmation($confirmation)
    {
        $this->confirmation = $confirmation;
        return $this;
    }

    /**
     * Get confirmation
     *
     * @return string 
     */
    public function getConfirmation()
    {
        return $this->confirmation;
    }
     /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Reservation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }


     public function __toString(){
        $locataire=$this->getNomClient();
        $surface=$this->getSurface();
        return $surface."-".$locataire;
    }
    
}
