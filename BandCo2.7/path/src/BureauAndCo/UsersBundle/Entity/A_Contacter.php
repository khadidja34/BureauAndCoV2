<?php

namespace BureauAndCo\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * A_Contacter
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="nom", message="cette personne est déja dans le système!.")
 */
class A_Contacter extends Personne
{
  
    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=255)
     */
    private $fonction;

    /**
     * Set fonction
     *
     * @param string $fonction
     * @return A_Contacter
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string 
     */
    public function getFonction()
    {
        return $this->fonction;
    }
}
