<?php

namespace BureauAndCo\UsersBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Serializable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\Table;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\User\EquatableInterface;

 
/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"user"="User","administrateur" = "Administrateur", "assistant" =   "Assistant"})
 * @UniqueEntity(fields="username", message="Ce pseudo est déja pris!")
 * @UniqueEntity (fields ="email", message =" Vous êtes déja inscrit(e)!")
 */
class User implements AdvancedUserInterface,Serializable
{
  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  
  /**
   * @ORM\Column(name="compte", type="string", length=30)
   * @Assert\NotBlank(message="Veuillez choisir le type SVP.")
   */
  private $compte;
  /**
   * @ORM\Column(name="username", type="string", length=255, unique=true)
   * @Assert\Length(min=5, minMessage="Le pseudo doit faire au moins {{ limit }} caractères.")
   */
  private $username;
  /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, unique=true)
     * @Assert\NotBlank(message="Ce champ est obligatoire!")
     * @Assert\Email(checkMX=true,message=" Veuillez saisir un Email valid SVP")
     */
    private $email;
 
  /**
   * @ORM\Column(name="password", type="string", length=255)
   * @Assert\NotBlank(message="Veuillez remplir ce champs SVP")
   * @Assert\Length(min=5, minMessage="Utiliser au moins{{ limit }} caractères.")
   * 
   */
  private $password;
  /**
   * @ORM\Column(name="salt", type="string", length=255)
   */
  private $salt;
  /**
   * @ORM\Column(name="roles", type="array")
   */
  private $roles;
   /**
    * @var boolean $isActive
    *
    * @ORM\Column(name="isActive", type="boolean")
    */
    private $isActive;
     /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\UsersBundle\Entity\Tache", mappedBy="user", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
      */
    private $taches;
    
 
  public function __construct()
  {
   
    $this->isActive = true;
    $this->roles = array('ROLE_ASSISTANT');
    $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);

  }
 
  public function getId()
  {
    return $this->id;
  }
 
  public function setUsername($username)
  {
    $this->username = $username;
    return $this;
  }
 
  public function getUsername()
  {
    return $this->username;
  }
  
   /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }
 
  public function getPassword()
  {
    return $this->password;
  }
 
  public function setSalt($salt)
  {
    $this->salt = $salt;
    return $this;
  }
 
  public function getSalt()
  {
    //return $this->salt;
      return $this->salt;

  }
 
  public function setRoles(array $roles)
  {
    $this->roles = $roles;
    return $this;
  }
  /**
  * @see RoleInterface
  */
    public function getRoles()
    {
        return $this->roles;
    }

 public function getIsActive()
  {
    return $this->isActive;
  }
 
  public function setIsActive($value)
  {
    $this->isActive = $value;
    return $this;
  }
  public function eraseCredentials()
  {
    $this->password = '';
  }
  function isEqualTo(UserInterface $user)
   { 
     if ($this->getId() == $user->getId())
        {
            return true;
        }

        else
        {
            return false;
        }
   }

  public function serialize()
{
    return serialize(array(
        $this->id,
        $this->compte,
        $this->username,
        $this->email,
        $this->password,
        $this->salt,
        $this->isActive,

    ));
}

public function unserialize($serialized)
{
    list (
        $this->id,
        $this->compte,
        $this->username,
        $this->email,
        $this->password,
        $this->salt,
        $this->isActive,
    ) = unserialize($serialized);
}
public function isEnabled() 
{        
    return $this->getIsActive();
}
public function isAccountNonExpired() 
{        
    return true;
}
public function isAccountNonLocked() 
{        
    return true;
}
public function isCredentialsNonExpired() 
{        
    return true;
}
     public function __toString(){
        return $this->getUsername();
    }
    

    /**
     * Set compte
     *
     * @param string $compte
     * @return User
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;

        return $this;
    }

    /**
     * Get compte
     *
     * @return string 
     */
    public function getCompte()
    {
        return $this->compte;
    }

    /**
     * Add taches
     *
     * @param \BureauAndCo\UsersBundle\Entity\Tache $taches
     * @return User
     */
    public function addTach(\BureauAndCo\UsersBundle\Entity\Tache $taches)
    {
        $this->taches[] = $taches;

        return $this;
    }

    /**
     * Remove taches
     *
     * @param \BureauAndCo\UsersBundle\Entity\Tache $taches
     */
    public function removeTach(\BureauAndCo\UsersBundle\Entity\Tache $taches)
    {
        $this->taches->removeElement($taches);
    }

    /**
     * Get taches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTaches()
    {
        return $this->taches;
    }
}
