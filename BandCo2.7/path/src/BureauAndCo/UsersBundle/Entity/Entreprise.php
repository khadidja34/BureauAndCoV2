<?php

namespace BureauAndCo\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Entreprise Client
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="nom", message="Ce cLient est déja dans le système");
 * @UniqueEntity(fields="numSiret", message="Ce cLient est déja dans le système.")
 * @UniqueEntity(fields="numTva", message="Ce cLient est déja dans le système.")
 * @UniqueEntity(fields="numTva", message="Ce siege social est attribué à une autre ntreprise!.")
 */
class Entreprise extends Client
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="numSiret", type="string", length=255)
     */
    private $numSiret;

    /**
     * @var string
     *
     * @ORM\Column(name="numTva", type="string", length=255)
     */
    private $numTva;

    /**
     * @var string
     *
     * @ORM\Column(name="siegeSocial", type="string", length=255)
     */
    private $siegeSocial;
      /**
     * @var string
     *
     * @ORM\Column(name="forme_juridique", type="string", length=255)
     */
    private $formeJuridique;
     /**
     * @var string
     *
     * @ORM\Column(name="capital", type="string", length=255)
     */
    private $capital;
    /**
     * @ORM\OneToOne(targetEntity="BureauAndCo\UsersBundle\Entity\Representant",cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $representant;
     /**
     * @ORM\OneToOne(targetEntity="BureauAndCo\UsersBundle\Entity\A_Contacter",cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $personneAcontacter;
  
      /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\SiteBundle\Entity\Location", mappedBy="locataire", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
   */
      //une entreprise a plusieurs locations
    private $location;
    

 public function __construct()

    {
            Parent::__construct();
            $this->location= new \Doctrine\Common\Collections\ArrayCollection();
    
    }

    /**
     * Set numSiret
     *
     * @param string $numSiret
     * @return Entreprise
     */
    public function setNumSiret($numSiret)
    {
        $this->numSiret = $numSiret;

        return $this;
    }

    /**
     * Get numSiret
     *
     * @return string 
     */
    public function getNumSiret()
    {
        return $this->numSiret;
    }

    /**
     * Set numTva
     *
     * @param string $numTva
     * @return Entreprise
     */
    public function setNumTva($numTva)
    {
        $this->numTva = $numTva;

        return $this;
    }

    /**
     * Get numTva
     *
     * @return string 
     */
    public function getNumTva()
    {
        return $this->numTva;
    }

    /**
     * Set siegeSocial
     *
     * @param string $siegeSocial
     * @return Entreprise
     */
    public function setSiegeSocial($siegeSocial)
    {
        $this->siegeSocial = $siegeSocial;

        return $this;
    }

    /**
     * Get siegeSocial
     *
     * @return string 
     */
    public function getSiegeSocial()
    {
        return $this->siegeSocial;
    }
     public function setRepresentant(Representant $representant = null)
      {
        $this->representant = $representant;
      }

      public function getRepresentant()
      {
        return $this->representant;
      }
        

         public function setPersonneAcontacter(A_Contacter $personneAcontacter = null)
      {
        $this->personneAcontacter = $personneAcontacter;
      }

      public function getPersonneAcontacter()
      {
        return $this->personneAcontacter;
      }

    /**
     * Add location
     *
     * @param \BureauAndCo\SiteBundle\Entity\NewLocation $location
     * @return Entreprise
     */
    public function addLocation(\BureauAndCo\SiteBundle\Entity\Location $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \BureauAndCo\SiteBundle\Entity\NewLocation $location
     */
    public function removeLocation(\BureauAndCo\SiteBundle\Entity\Location $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set formeJuridique
     *
     * @param string $formeJuridique
     * @return Entreprise
     */
    public function setFormeJuridique($formeJuridique)
    {
        $this->formeJuridique = $formeJuridique;

        return $this;
    }

    /**
     * Get formeJuridique
     *
     * @return string 
     */
    public function getFormeJuridique()
    {
        return $this->formeJuridique;
    }

    /**
     * Set capital
     *
     * @param string $capital
     * @return Entreprise
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string 
     */
    public function getCapital()
    {
        return $this->capital;
    }   
}
