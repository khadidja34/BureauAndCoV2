<?php

namespace BureauAndCo\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assistant User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Assistant extends User
{ 
   /**
    * @ORM\ManyToOne(targetEntity="BureauAndCo\SiteBundle\Entity\Site", inversedBy="assistants")
    * @ORM\JoinColumn(nullable=false)
    * 
    */
    private $site;

    /**
     * Set site
     *
     * @param \BureauAndCo\SiteBundle\Entity\Site $site
     * @return Assistant
     */
    public function setSite(\BureauAndCo\SiteBundle\Entity\Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \BureauAndCo\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }
}
