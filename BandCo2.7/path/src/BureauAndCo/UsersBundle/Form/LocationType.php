<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;




class LocationType extends AbstractType
{
  private $em;
     public function __construct($em)
     {
         $this->em=$em;
          // var_dump($em);
       
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surface', 'choice')
            ->add('dateDebut','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
            ->add('dateFin','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
            ->add('locataire','entity',array('class' => 'BureauAndCoUsersBundle:Entreprise',
                                             'choice_label' => 'nom',))
            ->add('site','entity', array(
    // query choices from this entity
                                      'class' => 'BureauAndCoSiteBundle:Site',
                                      'choice_label' => 'nom',
                                      'placeholder'=>'Choisissez un site ici',
                                       'attr'=>(array('class'=>'site'))))

            ->add('Valider','submit'); 


          $variable=function(FormInterface $form, $site){

           $listSurface=$this->em->getRepository('BureauAndCoSiteBundle:SurfaceExploite')->findBy(array('libre'=>'oui','site'=>$site));
          if ($listSurface){
          $surfaces=array();
          foreach ($listSurface as $surface) {
            $surfaces[$surface->getNum()]=$surface->getNum();
            }
         }
         else{
          $surfaces="Aucun résultat n'a été trouvé";
           }

           $form->add('surface','choice',array('attr'=>array('class'=>'s'),
                                         'choices'=>$surfaces));

        };
        $builder->get('site')->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)use ($variable){
         $variable($event->getForm()->getParent(),$event->getForm()->getData());
        });
    }
     

    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\Location'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_location';
    }
}
