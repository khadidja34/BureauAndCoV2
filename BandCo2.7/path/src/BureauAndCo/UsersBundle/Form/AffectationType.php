<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AffectationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('date')
            ->add('site','entity', array(
                                      'class' => 'BureauAndCoSiteBundle:Site',
                                      'choice_label' => 'nom'))
            ->add('admin','entity', array(
                                      'class' => 'BureauAndCoUsersBundle:Assistant',
                                      'choice_label' => 'username'))
            ->add('Valider','submit');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\Affectation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_affectation';
    }
}
