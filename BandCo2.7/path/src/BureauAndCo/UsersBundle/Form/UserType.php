<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
              ->add('compte', 'choice', array(
               'choices' => array(
                'Admin' => 'Administrateur',
                'Assistant' => 'Assistant',
                ),
               'required'    =>false,
               'placeholder' => 'Choisissez le type ici',
               'empty_data'  => null
               ))
            ->add('username')
            ->add('email')
            ->add('password')
            ->remove('salt')
            ->remove('roles')
            ->remove('isActive')
            ->add('Valider','submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_user';
    }
}
