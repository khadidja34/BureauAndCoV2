<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NouveauCompteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('compte', 'choice', array(
       'choices' => array(
        'Assistant' => 'Assistant',
        'Admin' => 'Administrateur',
        'Assistant' => 'Assistant',
        ),
       'required'    =>false,
       'placeholder' => 'Choisissez le type ici',
       'empty_data'  => null
       ))
      ->add('username','text',array('required' => false))
      ->add('email','email',array('required' => false))
      ->add('password','password',array('required' => false))
      ->add('site', 'entity', array(
                                      'class' => 'BureauAndCoSiteBundle:Site',
                                      'choice_label' => 'nom',
      ))
      ->add('Valider','submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
      return 'bureauandco_usersbundle_nouveau_compte';
    }
    
  }
