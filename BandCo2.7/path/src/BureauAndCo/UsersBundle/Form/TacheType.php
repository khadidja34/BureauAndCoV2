<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TacheType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom','text',array('attr' => array('placeholder' => ' Donnez un nom à cette tâche')))
            ->add('description','text',array('attr' => array('placeholder' => ' Décrire cette tâche')))
            ->remove('dateDebut')
        ->add('dateRappel')
        ->add('dateFin')
        ->add('urgence',  'choice', array(
                                                      'choices' => array(
                                                          'Urgent' => 'Urgent',
                                                          'Moyen' => 'Moyen',
                                                          'Faible' => 'Faible',
                                                         
                                                      ),
                                                      // 'required'    => false,
                                                      // 'placeholder' => 'Quelle surface cherchez vous?',
                                                      // 'empty_data'  => null
                                                  ))
        ->add('Ajouter', 'submit');
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\Tache'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_tache';
    }
}
