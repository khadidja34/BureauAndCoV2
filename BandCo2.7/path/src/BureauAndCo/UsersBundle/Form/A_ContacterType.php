<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class A_ContacterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fonction')
           ->add('Valider','submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\A_Contacter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_a_contacter';
    }
     public function getParent()
      {
        return new PersonneType();
      }
}
