<?php

namespace BureauAndCo\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class ReservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nomClient','entity', array(
    
                                      'class' => 'BureauAndCoUsersBundle:Client',
                                      'choice_label' => 'nom',
                                      'placeholder'=>'Choisissez un client ici',
                                       'attr'=>(array('class'=>'site'))))
            ->remove('site','entity', array(
    // query choices from this entity
                                      'class' => 'BureauAndCoSiteBundle:Site',
                                      'choice_label' => 'nom',
                                      'placeholder'=>'Choisissez un site ici',
                                       'attr'=>(array('class'=>'site'))))
            ->remove('surface','choice')
            ->add('dateDebut','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
            ->add('Valider','submit');   
    }
    
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\UsersBundle\Entity\Reservation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_usersbundle_reservation';
    }
}
