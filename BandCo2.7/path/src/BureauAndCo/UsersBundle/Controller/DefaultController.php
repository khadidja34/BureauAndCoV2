<?php

namespace BureauAndCo\UsersBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use BureauAndCo\UsersBundle\Form\EntrepriseType;
use BureauAndCo\UsersBundle\Form\PersonneType;
use BureauAndCo\SiteBundle\Form\SurfaceExploiteType;
use BureauAndCo\UsersBundle\Form\RepresentantType;
use BureauAndCo\UsersBundle\Form\A_ContacterType;
use BureauAndCo\UsersBundle\Entity\Personne;
use BureauAndCo\UsersBundle\Entity\Reservation;
use BureauAndCo\UsersBundle\Entity\Entreprise;
use BureauAndCo\UsersBundle\Entity\Affectation;
use BureauAndCo\UsersBundle\Entity\Client;
use BureauAndCo\UsersBundle\Entity\User;
use BureauAndCo\UsersBundle\Entity\Administrateur;
use BureauAndCo\UsersBundle\Entity\Assistant;
use BureauAndCo\UsersBundle\Entity\Prospect;
use BureauAndCo\SiteBundle\Entity\Location;
use BureauAndCo\UsersBundle\Form\ProspectType;
use BureauAndCo\UsersBundle\Form\UserType;
use BureauAndCo\UsersBundle\Form\EditUserType;
use BureauAndCo\UsersBundle\Form\NouveauCompteType;
use BureauAndCo\UsersBundle\Form\AffectationType;
use BureauAndCo\UsersBundle\Form\AdministrateurType;
use BureauAndCo\UsersBundle\Form\AssistantType;
use BureauAndCo\UsersBundle\Entity\Tache;
use BureauAndCo\UsersBundle\Form\TacheType;
use BureauAndCo\SiteBundle\Entity\Locataire;
use BureauAndCo\SiteBundle\Entity\SurfaceExploite;
use BureauAndCo\SiteBundle\Repository\SurfaceExploiteRepository;
use BureauAndCo\SiteBundle\Repository\RequestRepository;
use BureauAndCo\UsersBundle\Form\LocationType;
use BureauAndCo\UsersBundle\Entity\Representant;
use BureauAndCo\UsersBundle\Entity\A_Contacter;
use Symfony\Component\HttpFoundation\Response;	
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;



class DefaultController extends Controller
{
    /**
     * @Route("/login", name="login")
     * 
     */
    public function loginAction(Request $request)
  {
    $repository=$this
            ->getDoctrine()
            ->getManager()
            ->getRepository('BureauAndCoUsersBundle:Administrateur');
    $verif=count($repository->findAll());
    if($verif==0){
      $user=new Administrateur();
      $psw="B&CGestion2017!#";
      $user->setCompte("Admin");
      $user->setRoles(array('ROLE_ADMIN'));
      $user->setUsername("Anne Faverdin");
      // $user->setPassword("B&CGestion2017!#");
      $user->setEmail("af@bureauxandco.com");
      $factory = $this->get('security.encoder_factory');
      $encoder = $factory->getEncoder($user);
      $password = $encoder->encodePassword($psw, $user->getSalt());
      $user->setPassword($password);       
      $em=$this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush(); 
    }
    // Si le visiteur est déjà identifié, on le redirige vers l'accueil
    if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
      return $this->redirectToRoute('a');
    }
    
    
    $authenticationUtils = $this->get('security.authentication_utils');

    return $this->render('BureauAndCoUsersBundle:Default:loginAdmin.html.twig', array(
      'last_username' => $authenticationUtils->getLastUsername(),
      'error'         => $authenticationUtils->getLastAuthenticationError(),
    ));
  }
/**
* @Method({"POST"})
* @Route("/login_check", name="login_check")
*/
public function check(Request $request)
{
    return new Response("Authentication...");
}

/**
* @Method({"GET"})
* @Route("/logout", name="logout")
*/
public function logoutAction()
{
    try {
            $this->get("request")->getSession()->invalidate();
            $this->get("security.context")->setToken(null);
            return true;
        } catch (\Exception $e) {
            return false;
        }
 }
    /**
     * @Route("/accueil", name="a")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
    public function accueilAdminAction()
    {

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
         $surfaces = $this->getDoctrine()
         ->getRepository('BureauAndCoSiteBundle:SurfaceExploite')
         ->findBy(array('a_enfant'=>"non"));
    }
 else{
        $surfaces = $this->getDoctrine()
         ->getRepository('BureauAndCoSiteBundle:SurfaceExploite')
         ->findBy(array('a_enfant'=>"non",'site'=>$this->getUser()->getSite()));
     }
        return $this->render('BureauAndCoUsersBundle:Default:accueilAdmin.html.twig',array('surfaces'=>$surfaces));
    }
    /**
     * @Route("/index", name="index")
     */
    public function indexnAction()
    {  
        return $this->render('BureauAndCoUsersBundle:Default:index.html.twig');
    }


/**
     * @Route("/nouveauCompte", name="nouveauCompte") 
     * @Security("has_role('ROLE_ASSISTANT')")
 */
 public function nouveauCompteAction(Request $request)
 {
   // if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
   $title="Nouveau compte utilisteur";
   $message="";
   $entity=new Assistant();

   $form =$this->createForm(new NouveauCompteType(),$entity);
   if($this->get('request')->getMethod()=='POST'){
    $form->bind($this->get('request'));
    $type=$form['compte']->getData();
    $pseudo=$form['username']->getData();
    $email=$form['email']->getData();
    $password=$form['password']->getData();
    $site=$form['site']->getData();
    
     switch($type){
      case "Admin":
      $entity = new Administrateur();
      $entity->setRoles(array('ROLE_ADMIN'));
      break;
      //**ajouter un nouveau assistant
      case "Assistant":
      $entity = new Assistant();
      $entity->setRoles(array('ROLE_ASSISTANT'));

      break;
     } 
        if ($form->isValid()) {
      
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($form->get('password')->getData(), $entity->getSalt());
        $entity->setPassword($password);
        $entity->setCompte($type);
        $entity->setUsername($pseudo);
        $entity->setEmail($email);
        $entity->setPassword($password);
        if($type=="Assistant"){
           $entity->setSite($site);
        }

          // $message="form valide";
         $em = $this->getDoctrine()->getManager();
          $em->persist($entity);
          $em->flush();
          $request->getSession()
          ->getFlashBag()
          ->add('success', 'Le nouveau compte a été créé avec succes');
          return $this->redirect($this->generateUrl('gestionAssistant'));
        }
    
    }
return $this->render('BureauAndCoUsersBundle:Default:nouveauCompte.html.twig',array('title'=>$title, 'message'=>$message,'form'=>$form->createView()));
}
// else{
//    return new Response("Acces interdit à cette ressource");
//  }
// }
/**
  *
  *@Route("/gestionAssistant", name="gestionAssistant")
  *
  * 
  */
    public function gestionAssistantAction()
    { 
    if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
      $em=$this->getDoctrine()->getManager();                         
      $listeAssistant = $em->getRepository('BureauAndCoUsersBundle:User')->findAll();
       $paginator  = $this->get('knp_paginator');
     $listAssistant = $paginator->paginate($listeAssistant, $this->get('request')->query->get('page', 1), 3);
      return $this->render('BureauAndCoUsersBundle:Default:gestionAssistant.html.twig', array(
        'list'=>$listAssistant));
     }
   else{
     return new Response('Acces interdit à cette ressource');
      }
  }
/**
 * @Route("/desactiverCompte/{id}", name="desactiverCompte")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
    public function desactiverCompteAction(User $user)
      {
       if ($this->get('security.context')->isGranted('ROLE_ADMIN')){ 
        $user->setIsActive(false);
        $em=$this->getDoctrine()->getManager();
        $em->flush(); 
          return $this->redirect($this->generateUrl('gestionAssistant'));    
      }
      else{
        return new Response("Acces interdit à cette ressource");
      }
    }

 /**
 * @Route("/activerCompte/{id}", name="activerCompte")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

    public function activerCompteAction(User $user)
      { 
       if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
        $user->setIsActive(true);
        $em=$this->getDoctrine()->getManager();
        $em->flush(); 
          return $this->redirect($this->generateUrl('gestionAssistant'));    
      } 
      else{
        return new Response ("Acces interdit à cette ressource");
      }
    }
     /**
     * @Route("/editerCompte/{id}", name="editerCompte")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function editerCompteAction(Request $request, User $user)
   {
     if ($this->get('security.context')->isGranted('ROLE_ADMIN')){  
      $title="Modification";
     
         if($user->getCompte()=="Admin"){
             $form =$this->createForm(new AdministrateurType(), $user);
            $view='BureauAndCoUsersBundle:Default:nouveauAdmin.html.twig';
          }
         else{
            $form =$this->createForm(new AssistantType(), $user);
            $view='BureauAndCoUsersBundle:Default:nouveauAssistant.html.twig';
          }
  
     if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
       if ($form->isValid()) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($form->get('password')->getData(), $user->getSalt());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Modification effectuée avec succès');
                return $this->redirect($this->generateUrl('gestionAssistant'));
       }
     }
  
  return $this->render($view,array('title'=>$title, 'form' => $form->createView()));
  }
  else{
    return new Response("Acces interdit à cette ressource");
   }
  }

 /**
 * @Route("/supprimerAssistant/{id}", name="suprimerAssistant")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

    public function SupprimerAssistantAction(Request $request, User $user)
      { 
      if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
        $em=$this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush(); 
        $request->getSession()
                ->getFlashBag()
                ->add('success', 'Suppression effectuée avec succès');
                return $this->redirect($this->generateUrl('gestionAssistant'));       
      }
      else{
        return new Response ("Acces interdit à cette ressource");
      }
 }
/**
 * @Route("/preavis/{id}", name="preavis")
 * @Security("has_role('ROLE_ASSISTANT')")
 */
   public function preavisAction(Request $request, Location $location)
   {
     $title="Préavis pour la surface : ".$location->getSurface();
     $message="";
      $form=$this->createFormBuilder()

        ->add('dateFin','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
      ->add('Valider', 'submit')
      ->getForm();

      if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
       if ($form->isValid()) {
         $dateFin=$form['dateFin']->getData();
         if($location->getDateDebut()>$dateFin){
          $message="Une incohérence de date a été détectée!";
         }
         else{
           $location->setPreavis("oui");
           $location->setDateFin($dateFin);
          $em = $this->getDoctrine()->getManager();
          $em->flush();
          $request->getSession()
                ->getFlashBag()
                ->add('success', 'Préavis ajouté avec succès');
                return $this->redirect($this->generateUrl('listLocation'));
      }
    }
  }
   return $this->render('BureauAndCoUsersBundle:Default:preavis.html.twig',array('title'=>$title,'message'=>$message, 'form' => $form->createView()));
}
/**
 * @Route("/editerSite/{id}", name="editerSite")
 * @Security("has_role('ROLE_ADMIN') and has_role('ROLE_ASSISTANT')")
 */
   public function editSiteAction(Request $request, Site $site)
   {

    $form =$this->createForm(new SiteType(), $site);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
                ->getFlashBag()
                ->add('success', 'Modification effectuée avec succès');
                return $this->redirect($this->generateUrl('gestion'));
    }
    
  }
}
/**
 * @Route("/editerAssistant/{id}", name="editerAssistant")
 * @Security("has_role('ROLE_ASSISTANT')")
 */
   public function editAssistantAction(Request $request, Assistant $assistant)
   {
    if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
    $title="Modification du compte de : ".$assistant->getUsername();
    $form =$this->createForm(new AssistantType(), $assistant);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
                ->getFlashBag()
                ->add('success', 'Modification effectuée avec succès');
                return $this->redirect($this->generateUrl('gestionAssistant'));
    }
    
  }
  return $this->render('BureauAndCoUsersBundle:Default:nouveauAssistant.html.twig',array( 'form' => $form->createView(),'title'=>$title));
}
else{
  return new Response("Acces interdit à cette ressource");
  }
}
/**
 * @Route("/editerAdmin/{id}", name="editerAdmin")
 * @Security("has_role('ROLE_ASSISTANT')")
 */
   public function editAdminAction(Request $request, Administrateur $admin)
   {
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')){
    $title="Modification du compte de : ".$admin->getUsername();
    $form =$this->createForm(new AdministrateurType(), $admin);
    $form->remove('site');
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
                ->getFlashBag()
                ->add('success', 'Modification effectuée avec succès');
                return $this->redirect($this->generateUrl('gestionAssistant'));
    }
    
  }
  return $this->render('BureauAndCoUsersBundle:Default:nouveauAdmin.html.twig',array( 'form' => $form->createView(),'title'=>$title));
}
else{
  return new Response("Acces interdit à cette ressource");
 }
}   
    // /**
    //  * @Route("/index", name="index")
    //  */
    // public function indexnAction()
    // {  
    //     return $this->render('BureauAndCoUsersBundle:Default:index.html.twig');
    // }
/**
 * @Route("/supprimerTache/{id}", name="suprimerTache")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */
    public function SupprimerTacheAction(Tache $tache)
      { 
        $em=$this->getDoctrine()->getManager();
        $em->remove($tache);
        $em->flush(); 
          return $this->redirect($this->generateUrl('listeDesTaches'));     
      }
/**
 * @Route("/supprimerReservation/{id}", name="supprimerReservation")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */
    public function SupprimerReservationAction(Request $request, Reservation $reservation)
      { 
        $em=$this->getDoctrine()->getManager();
         $num=$reservation->getSurface();

         $repository=$this
            ->getDoctrine()
            ->getManager()
            ->getRepository('BureauAndCoUsersBundle:Reservation');
         $verif=$repository->findBy(array('surface'=>$num));
          if(count($verif)==1){
              $repository=$this
            ->getDoctrine()
            ->getManager()
            ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
            $surface=$repository->findOneBy(array('num'=>$num));
              if($surface){
                $surface->setLibre("oui");
              }
            }
        
             $em->remove($reservation);
             $em->flush(); 
        $request->getSession()
                ->getFlashBag()
                ->add('success', 'La réservation a été annulée');
       return $this->redirect($this->generateUrl('gestion'));
              
      }
 /**
 * @Route("/supprimerLocation/{id}", name="supprimerLocation")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
    public function supprimerLocationAction(Request $request, Location $location)
      { 
       
         $em=$this->getDoctrine()->getManager();
         $num=$location->getSurface();
         $client=$location->getLocataire();
         $rep=$this
            ->getDoctrine()
            ->getManager()
            ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
          $surface = $rep->findOneBy(array('num'=>$num));
          $surface->setLibre("oui");
           $rep=$this
            ->getDoctrine()
            ->getManager()
            ->getRepository('BureauAndCoSiteBundle:Location');
          $verif = $rep->findBy(array('locataire'=>$location->getLocataire()));
          $em->remove($location);
          $em->flush(); 
          if(count($verif==1)){
             $prospect=new Prospect();
              $prospect->setNom($client->getNom());
              $prospect->setActivite($client->getActivite());
              $prospect->setEmail($client->getPersonneAcontacter()->getEmail());
              $prospect->setTel($client->getPersonneAcontacter()->getTel());
              $prospect->setVille($client->getSiegeSocial());
              $em->remove($client);
              $em->flush(); 
              $em->persist($prospect);
              $em->flush($prospect);
               $request->getSession()
                ->getFlashBag()
                ->add('success', 'La location a été supprimée, la surface est libre, ce client est devenu prospect');
          return $this->redirect($this->generateUrl('mesProspects'));

          }
          else{
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'La location a été supprimée et la surface est désormais libre');
          return $this->redirect($this->generateUrl('listLocation'));
          }  
      }

 /**
     * @Route("/nouveauLocataire", name="nouveauLocataire")
     * @Security("has_role('ROLE_ASSISTANT')")
*/
    public function nouveauLocataireAction(Request $request)
    {
    	 $locataire = new Entreprise();
      $form = $this->createForm(new EntrepriseType(), $locataire);

	    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
	     if ($form->isValid()) {
         
          $em = $this->getDoctrine()->getManager();
          $em->persist($locataire);
          $em->persist($locataire->getRepresentant());
          $em->persist($locataire->getPersonneAcontacter());
          $em->flush();
          $request->getSession()
                ->getFlashBag()
                ->add('success', 'Le nouveau client a été bien enregistré, Merci d\'établir son contrat de location');
            return $this->redirect($this->generateUrl('gestionClient'));
        }   
      }
      return $this->render('BureauAndCoUsersBundle:Default:nouveauLocataire.html.twig',array( 'form' => $form->createView()));
    }
      /**
     * @Route("/nouveauProspect", name="nouveauProspect")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
    public function nouveauProspecteAction(Request $request)
    {
       $prospect = new Prospect();
         $form = $this->createForm(new ProspectType(), $prospect);

      if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
       if ($form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($prospect);
          $em->flush();
          $request->getSession()
                ->getFlashBag()
                ->add('success', 'Le nouveau client prospect a été bien enregistré, Merci d\'établir son contrat de réservation');
            return $this->redirect($this->generateUrl('listProspect'));
        }   
      }
      return $this->render('BureauAndCoUsersBundle:Default:nouveauProspect.html.twig',array( 'form' => $form->createView()));
    }
     /**
     * @Route("/nouvelleTache", name="nouvelleTache")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
    public function nouvelleTacheAction(Request $request)
    {
         // $user=$this->container->get('security.context')->getToken()->getUser();
         $tache = new Tache();
         $tache->setUser($this->getUser());
         $form = $this->createForm(new TacheType(), $tache);
      if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
       if ($form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($tache);
          $em->flush();
          return $this->redirect($this->generateUrl('listeDesTaches'));
        }   
      }
      return $this->render('BureauAndCoUsersBundle:Default:nouvelleTache.html.twig',array( 'form' => $form->createView()));
    }
/**
  *
  * @Route("/mesProspects", name="mesProspects")
  * @Security("has_role('ROLE_ASSISTANT')")
*/
    public function mesProspectAction()
    { 
    
      $listProspect=array();
      $em=$this->getDoctrine()->getManager(); 
      $listProspect = $em->getRepository('BureauAndCoUsersBundle:Prospect')->findAll();
      return $this->render('BureauAndCoUsersBundle:Default:mesProspect.html.twig', array(
        'list'=>$listProspect));
      }
 /**
  * @Route("/gestionClient", name="gestionClient")
  * @Security("has_role('ROLE_ASSISTANT')")
  */
    public function gestionClientAction()
    { 
      $listSites=array();
      $listClient=array();
      $em=$this->getDoctrine()->getManager(); 
      if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
        $listClient = $em->getRepository('BureauAndCoUsersBundle:Entreprise')->findAll();
      }
      elseif($this->get('security.context')->isGranted('ROLE_ASSISTANT')) {
         $listClient = $em->getRepository('BureauAndCoSiteBundle:Locataire')->findBy(array('site'=>$this->getUser()->getSite()));
      }                      
      return $this->render('BureauAndCoUsersBundle:Default:gestionClient.html.twig', array(
        'list'=>$listClient));
}
/**
 * @Route("/supprimerClient/{id}", name="supprimerClient")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */

 public function SupprimerClientAction(Request $request, Entreprise $client)
 { 
  $em=$this->getDoctrine()->getManager();
  $prospect=new Prospect();
  $prospect->setNom($client->getNom());
  $prospect->setActivite($client->getActivite());
  $prospect->setEmail($client->getPersonneAcontacter()->getEmail());
  $prospect->setTel($client->getPersonneAcontacter()->getTel());
  $prospect->setVille($client->getSiegeSocial());
  $em->remove($client);
  $em->flush(); 
  $em->persist($prospect);
  $em->flush($prospect);
 
  $request->getSession()
          ->getFlashBag()
          ->add('success', 'Le client est passé à prospect');
  return $this->redirect($this->generateUrl('mesProspects'));  
}
/**
 * @Route("/supprimerProspect/{id}", name="supprimerProspect")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */

 public function SupprimerProspectAction(Request $request,Prospect $client)
 { 
  $em=$this->getDoctrine()->getManager();
  $em->remove($client);
  $em->flush(); 
 
  $request->getSession()
          ->getFlashBag()
          ->add('success', 'Le client est supprimé');
  return $this->redirect($this->generateUrl('mesProspects'));  
}

/**
     * @Route("/editerClient/{id}", name="editerClient")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
   public function editClientAction(Request $request, Entreprise $entreprise)
   {
    $form =$this->createForm(new EntrepriseType(), $entreprise);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {  
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
          ->getFlashBag()
          ->add('success', 'La Modification a été enregistrée avec succés');
     return $this->redirect($this->generateUrl('gestionClient'));
    }
    
  }
    return $this->render('BureauAndCoUsersBundle:Default:nouveauLocataire.html.twig',array( 'form' => $form->createView()));
}
/**
     * @Route("/editerProspect/{id}", name="editerProspect")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
   public function editProspectAction(Request $request, Prospect $prospect)
   {
    $form =$this->createForm(new ProspectType(), $prospect);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {  
      $em = $this->getDoctrine()->getManager();
      $em->flush();
       $request->getSession()
          ->getFlashBag()
          ->add('success', 'La Modification a été enregistrée avec succés');
    return $this->redirect($this->generateUrl('mesProspects'));
    }
  }
    return $this->render('BureauAndCoUsersBundle:Default:nouveauProspect.html.twig',array( 'form' => $form->createView()));
}

/**
 * @Route("/infoClient/{id}", name="infoClient")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
*/
  public function infoClientAction(Client $client)
     { 
      $client = $this->getDoctrine()
      ->getRepository('BureauAndCoUsersBundle:Client')
      ->find($client);
      $representant=$client->getRepresentant();
      $contact=$client->getPersonneAcontacter();
     
     return $this->render('BureauAndCoUsersBundle:Default:infoClient.html.twig',array( 'client' => $client,'representant'=>$representant,'contact'=>$contact));
}
/**
 * @Route("/infoLocation/{id}", name="infoLocation")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
     public function infoLocationAction(Location $location)
     { 
      $myLocation = $this->getDoctrine()
      ->getRepository('BureauAndCoSiteBundle:Location')
      ->find($location);
     return $this->render('BureauAndCoSiteBundle:Default:infoLocation.html.twig',array( 'location' => $myLocation));
   }
/**
 * @Route("/infoReservation/{id}", name="infoReservation")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
*/
     public function infoReservationAction(Reservation $reservation)
     { 
      $myReservation = $this->getDoctrine()
      ->getRepository('BureauAndCoUsersBundle:Reservation')
      ->find($reservation);
       return $this->render('BureauAndCoSiteBundle:Default:infoReservation.html.twig',array( 'reservation' => $myReservation));
   }
   /**
     * @Route("/editerTache/{id}", name="editerTache")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function editerTacheAction(Request $request, Tache $tache)
   {
   $rep=$this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoUsersBundle:Tache');
         $taches = $rep->findBy(array('user'=>$this->getUser(),'id'=>$tache->getId()));
    if(count($taches)==0){
      return new Response("Acces interdit à cette ressource");
    }
    else{
    $form =$this->createForm(new TacheType(), $tache);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
                ->getFlashBag()
                ->add('success', 'Modification effectuée avec succès');
      return $this->redirect($this->generateUrl('listeDesTaches'));
    }
   }
  }
  return $this->render('BureauAndCoUsersBundle:Default:nouvelleTache.html.twig',array( 'form' => $form->createView()));
}
   /**
     * @Route("/contrat/{id}", name="contrat")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function contrat(Location $location)
    {
      $em=$this->getDoctrine()->getManager();                         
      $mylocation = $em->getRepository('BureauAndCoSiteBundle:Location')->find($location);

        $html = $this->container->get('templating')->render('BureauAndCoSiteBundle:Default:contrat.html.twig',array('location'=>$mylocation));  
      //on appelle le service html2pdf
      $html2pdf = $this->get('html2pdf_factory')->create();
      //real : utilise la taille réelle
      $html2pdf->pdf->SetDisplayMode('real');
      //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
      $html2pdf->writeHTML($html);
      //Output envoit le document PDF au navigateur internet
      return new Response($html2pdf->Output('contrat.pdf'), 200, array('Content-Type' => 'application/pdf'));
   }
    
}