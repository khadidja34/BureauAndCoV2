<?php

namespace BureauAndCo\SiteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 *SalleReunion SurfaceExploite
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="num", message="Cette salle de réunion existe déja, merci de vérifier le numéro saisi!.")
 */
class SalleReunion extends SurfaceExploite
{
    
}
