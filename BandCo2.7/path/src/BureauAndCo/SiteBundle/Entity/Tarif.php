<?php

namespace BureauAndCo\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Tarif
 *
 * @ORM\Table(name="tarif")
 * @ORM\Entity(repositoryClass="BureauAndCoSiteBundle\Repository\TarifRepository")
 * 
 */
class Tarif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
   
    /**
     * @var string
     *
     * @ORM\Column(name="devise", type="string", length=60)
     * @Assert\NotBlank(message="ce champs est obligatoire.")
     
     */

    private $devise;

    /**
     * @var float
     *
     * @ORM\Column(name="heure", type="float", nullable=false)
     * @Assert\NotBlank(message="ce champs est obligatoire.")
     */
    private $heure;

    /**
     * @var float
     *
     * @ORM\Column(name="demijournee", type="float", nullable=false)
     * @Assert\NotBlank(message="ce champs est obligatoire.")
     */
    private $demijournee;

    /**
     * @var float
     *
     * @ORM\Column(name="journee", type="float", nullable=false)
     * @Assert\NotBlank(message="ce champs est obligatoire.")
     */
    private $journee;

    /**
     * @var float
     *
     * @ORM\Column(name="mois", type="float", nullable=false)
     * @Assert\NotBlank(message="ce champs est obligatoire.")
     */
    private $mois;
    
    
public function __construct()

     {
        $this->devise="Euro";
     }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
// **********************************************************
    /**
     * Set typeSuraface
     *
     * @param string $typeSurface
     *
     *
     */
    public function setTypeSurface($typeSurface)
    {
        $this->typeSurface = $typeSurface;

        return $this;
    }

    /**
     * Get typeSurface
     *
     * @return string
     */
    public function getTypeSurface()
    {
        return $this->typeSurface;
    }
    //**********************************************************
    /**
     * Set devise
     *
     * @param string $devise
     *
     *
     */
    public function setDevise($devise)
    {
        $this->devise = $devise;

        return $this;
    }

    /**
     * Get devise
     *
     * @return string
     */
    public function getDevise()
    {
        return $this->devise;
    }

    /**
     * Set heure
     *
     * @param float $heure
     *
     * @return Tarif
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return float
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set demijournee
     *
     * @param float $demijournee
     *
     * @return Tarif
     */
    public function setDemijournee($demijournee)
    {
        $this->demijournee = $demijournee;

        return $this;
    }

    /**
     * Get demijournee
     *
     * @return float
     */
    public function getDemijournee()
    {
        return $this->demijournee;
    }

    /**
     * Set journee
     *
     * @param float $journee
     *
     * @return Tarif
     */
    public function setJournee($journee)
    {
        $this->journee = $journee;

        return $this;
    }

    /**
     * Get journee
     *
     * @return float
     */
    public function getJournee()
    {
        return $this->journee;
    }

    /**
     * Set mois
     *
     * @param float $mois
     *
     * @return Tarif
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return float
     */
    public function getMois()
    {
        return $this->mois;
    }
    // public function __toArray(){
    //     $mois=$this->getMois();
    //     $demijournee=$this->getDemijournee();
    //     $heure=$this->getHeure();
    // }
    // public function __toString(){
    //     return $this->getDevise();
    // }
     /**
     * Set site
     */
    public function setSite(Site $site)
      {
        $this->site = $site;

        return $this;
      }
     /**
     * Get site
     */
      public function getSite()
      {
        return $this->site;
      }
      public function __toString(){
       $heure=$this->getHeure();
       $demijournee=$this->getDemijournee();
       $journee=$this->getJournee();
       $mois=$this->getMois();
       $devise=$this->getDevise();
        // return " Heure : ".$heure.$devise.'<br>'.'Demi-journée:'.$demijournee.$devise.'<br>'.'Journée: '.$journee.$devise;

        return "Heure: ".$heure.' '.$devise.','." Demi-journée : ".$demijournee." ".$devise."," ." Journée : ".$journee." ".$devise.","." Mois : ".$mois." ".$devise." ";
        // return $this->getDevise();
    }
}

