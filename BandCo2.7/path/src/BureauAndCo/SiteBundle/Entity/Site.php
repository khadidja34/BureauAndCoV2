<?php

namespace BureauAndCo\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Site
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="nom", message="Un site avec ce nom existe déja!.")
 * @UniqueEntity(fields="adresse", message="vous avez déja un site à cette adresse!.")
 */
class Site
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Ce champ est obligatoire")

     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     * @Assert\Length(min=3, minMessage="ce champs doit faire au moins {{ limit }} caractères.")
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     * @Assert\Length(min=3, minMessage="ce champs doit faire au moins {{ limit }} caractères.")
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255)
     */
    private $pays;
    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=15)
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     * @Assert\Length(max=15, min=7,maxMessage="ce champs doit faire au max {{ limit }} caractères.",minMessage="ce champs doit faire au moins {{ limit }} caractères.")
     */
    private $tel;
    /**
     * @var int
     *
     * @ORM\Column(name="superficie", type="integer")@Assert\Range(max=50, maxMessage="le nombre des bureaux doit être entre 0 et 50!")
     @Assert\Length(min=1, minMessage="ce champs ne peut pas être vide!")
     */

    private $superficie;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_bureau", type="integer")
     * @Assert\Range(max=50, maxMessage="le nombre des bureaux doit être entre 0 et 50!")
     */
    private $nbBureau;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_salle_reunion", type="integer")
     * @Assert\Range(max=10, maxMessage="le nombre des salles de Réunion doit être entre 0 et 10!")
     */
    private $nbSalleReunion;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_salle_formation", type="integer")
     * @Assert\Range(max=10, maxMessage="le nombre des salles de Réunion doit être entre 0 et 10!")
     * @Assert\Range(max=10, maxMessage="le nombre des salles de Réunion doit être entre 0 et 10!")
     */
    private $nbSalleFormation;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_boite_postale", type="integer")
     * @Assert\Range(max=50, maxMessage="le nombre des boîtes postales doit être entre 0 et 50!")
     */
    private $nbBoitePostale;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_parking", type="integer")
     * @Assert\Range(max=3, maxMessage="le nombre des parking doit être entre 0 et 3!")
     */
    private $nbParking;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_coworking", type="integer")
     ** @Assert\Range(max=10, maxMessage="le nombre espaces co-working doit être entre 0 et 10!")
     */
    private $nbCoworking;
     /**
    * @ORM\Column(name="date", type="datetime")
    */

     private $date;
     
    /**
   * @ORM\ManyToOne(targetEntity="BureauAndCo\SiteBundle\Entity\Categorie",inversedBy="sites")
   * @ORM\JoinColumn(nullable=false)
   */
    private $categorie;
     /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\SiteBundle\Entity\SurfaceExploite", mappedBy="site", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
   */
     private $surfaces;
    
    /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\SiteBundle\Entity\Location", mappedBy="site", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
   */
    private $locations;
      /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\UsersBundle\Entity\Reservation", mappedBy="site", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
   */
      private $reservations;
      
     /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\UsersBundle\Entity\Assistant", mappedBy="site", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
   */
      private $assistants;
     public function __construct()

     {
        $this->surfaces= new \Doctrine\Common\Collections\ArrayCollection();
        $this->assistants= new \Doctrine\Common\Collections\ArrayCollection();
        $this->locations= new \Doctrine\Common\Collections\ArrayCollection();
        $this->reservations= new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \Datetime();
        
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Site
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Set categorie
     */
    public function setCategorie(Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }
     /**
     * Get categorie
     */
     public function getCategorie()
     {
        return $this->categorie;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Site
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Site
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Site
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set superficie
     *
     * @param integer $superficie
     *
     * @return Site
     */
    public function setSuperficie($superficie)
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * Get superficie
     *
     * @return int
     */
    public function getSuperficie()
    {
        return $this->superficie;
    }

    /**
     * Set nbBureau
     *
     * @param integer $nbBureau
     *
     * @return Site
     */
    public function setNbBureau($nbBureau)
    {
        $this->nbBureau = $nbBureau;

        return $this;
    }

    /**
     * Get nbBureau
     *
     * @return int
     */
    public function getNbBureau()
    {
        return $this->nbBureau;
    }

    /**
     * Set nbSalleReunion
     *
     * @param integer $nbSalleReunion
     *
     * @return Site
     */
    public function setNbSalleReunion($nbSalleReunion)
    {
        $this->nbSalleReunion = $nbSalleReunion;

        return $this;
    }

    /**
     * Get nbSalleReunion
     *
     * @return int
     */
    public function getNbSalleReunion()
    {
        return $this->nbSalleReunion;
    }

    /**
     * Set nbSalleFormation
     *
     * @param integer $nbSalleFormation
     *
     * @return Site
     */
    public function setNbSalleFormation($nbSalleFormation)
    {
        $this->nbSalleFormation = $nbSalleFormation;

        return $this;
    }

    /**
     * Get nbSalleFormation
     *
     * @return int
     */
    public function getNbSalleFormation()
    {
        return $this->nbSalleFormation;
    }

    /**
     * Set nbBoitePostale
     *
     * @param integer $nbBoitePostale
     *
     * @return Site
     */
    public function setNbBoitePostale($nbBoitePostale)
    {
        $this->nbBoitePostale = $nbBoitePostale;

        return $this;
    }

    /**
     * Get nbBoitePostale
     *
     * @return int
     */
    public function getNbBoitePostale()
    {
        return $this->nbBoitePostale;
    }

    /**
     * Set nbParking
     *
     * @param integer $nbParking
     *
     * @return Site
     */
    public function setNbParking($nbParking)
    {
        $this->nbParking = $nbParking;

        return $this;
    }

    /**
     * Get nbParking
     *
     * @return int
     */
    public function getNbParking()
    {
        return $this->nbParking;
    }

    /**
     * Set nbCoworking
     *
     * @param integer $nbCoworking
     *
     * @return Site
     */
    public function setNbCoworking($nbCoworking)
    {
        $this->nbCoworking = $nbCoworking;

        return $this;
    }

    /**
     * Get nbCoworking
     *
     * @return int
     */
    public function getNbCoworking()
    {
        return $this->nbCoworking;
    }
    public function setDate(\Datetime $date)
    {
        $this->date = $date;
        return $this;
    }
    public function getDate()
    {
        return $this->date;
    }
     /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Site
     */
     public function setTel($tel)
     {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }     
    public function __toString()
    {
        return $this->getNom();
    }
    

    /**
     * Add surfaces
     *
     * @param \BureauAndCo\SiteBundle\Entity\SurfaceExploite $surfaces
     * @return Site
     */
    public function addSurface(\BureauAndCo\SiteBundle\Entity\SurfaceExploite $surfaces)
    {
        $this->surfaces[] = $surfaces;

        return $this;
    }

    /**
     * Remove surfaces
     *
     * @param \BureauAndCo\SiteBundle\Entity\SurfaceExploite $surfaces
     */
    public function removeSurface(\BureauAndCo\SiteBundle\Entity\SurfaceExploite $surfaces)
    {
        $this->surfaces->removeElement($surfaces);
    }

    /**
     * Get surfaces
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSurfaces()
    {
        return $this->surfaces;
    }

    /**
     * Add locations
     *
     * @param \BureauAndCo\SiteBundle\Entity\NewLocation $locations
     * @return Site
     */
    public function addLocation(\BureauAndCo\SiteBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \BureauAndCo\SiteBundle\Entity\NewLocation $locations
     */
    public function removeLocation(\BureauAndCo\SiteBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add reservations
     *
     * @param \BureauAndCo\UsersBundle\Entity\Reservation $reservations
     * @return Site
     */
    public function addReservation(\BureauAndCo\UsersBundle\Entity\Reservation $reservations)
    {
        $this->reservations[] = $reservations;

        return $this;
    }

    /**
     * Remove reservations
     *
     * @param \BureauAndCo\UsersBundle\Entity\Reservation $reservations
     */
    public function removeReservation(\BureauAndCo\UsersBundle\Entity\Reservation $reservations)
    {
        $this->reservations->removeElement($reservations);
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservations()
    {
        return $this->reservations;
    }

 /**
     * Add users
     *
     * @param \BureauAndCo\UsersBundle\Entity\Assistant $assistants
     * @return Assistant
     */
    public function addAssistant(\BureauAndCo\UsersBundle\Entity\Assistant $assistant)
    {
        $this->assistants[] = $assistants;

        return $this;
    }

    /**
     * Remove assistants
     *
     * @param \BureauAndCo\UsersBundle\Entity\Assistant $assistant
     */
    public function removeAssistant(\BureauAndCo\UsersBundle\Entity\Assistant $assistant)
    {
        $this->assistants->removeAssistant($assistant);
    }

    /**
     * Get assistant
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssistants()
    {
        return $this->assistants;
    }

}
