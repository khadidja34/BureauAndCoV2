<?php

namespace BureauAndCo\SiteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
/**
 *Locataire Location
 *
 * @ORM\Entity()
 * 
 */
class Locataire extends Location
{
     /**
     * @var string
     *
     * @ORM\Column(name="surface", type="string", length=255)
     */
    private $surface;
    

    /**
     * Set surface
     *
     * @param string $surface
     * @return Locataire
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return string 
     */
    public function getSurface()
    {
        return $this->surface;
    }
}
