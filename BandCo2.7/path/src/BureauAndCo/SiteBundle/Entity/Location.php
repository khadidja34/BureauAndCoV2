<?php

namespace BureauAndCo\SiteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use BureauAndCo\SiteBundle\Entity\Site;
use BureauAndCo\SiteBundle\Entity\SurfaceExploite;
use BureauAndCo\UsersBundle\Entity\Client;
use BureauAndCo\UsersBundle\Entity\Entreprise;
/**
 * Location
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"location"="Location","locataire"="Locataire"})
 */
class Location
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
  /**
     * @ORM\ManyToOne(targetEntity="BureauAndCo\UsersBundle\Entity\Entreprise",inversedBy="location")
     * @ORM\JoinColumn(nullable=false)
  */
    //Un locataire peut avoir plusieur contrat de location ou plusieurs contrat de location peuvent etre attribuées à un locataire
    private $locataire;
     /**
       * @ORM\ManyToOne(targetEntity="BureauAndCo\SiteBundle\Entity\Site",inversedBy="locations")
       * @ORM\JoinColumn(nullable=false)
    */
    //Un locataire peut louer plusieurs surfaces à la fois
    private $site;
    /**
    * @ORM\Column(name="date_debut", type="datetime")
    */
    private $dateDebut;
      /**
    * @ORM\Column(name="preavis", type="string", length=50)
    */
    private $preavis;
    /**
    * @ORM\Column(name="date_fin", type="datetime")
    */
    private $dateFin;
     public function __construct()
   {
        $this->preavis = "non indiqué";
        $this->dateFin = new \DateTime();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Location
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set locataire
     *
     * @param \BureauAndCo\UsersBundle\Entity\Entreprise $locataire
     * @return Location
     */
    public function setLocataire(\BureauAndCo\UsersBundle\Entity\Entreprise $locataire)
    {
        $this->locataire = $locataire;

        return $this;
    }

    /**
     * Get locataire
     *
     * @return \BureauAndCo\UsersBundle\Entity\Entreprise 
     */
    public function getLocataire()
    {
        return $this->locataire;
    }

    /**
     * Set site
     *
     * @param \BureauAndCo\SiteBundle\Entity\Site $site
     * @return Location
     */
    public function setSite(\BureauAndCo\SiteBundle\Entity\Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \BureauAndCo\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set preavis
     *
     * @param string $preavis
     * @return Location
     */
    public function setPreavis($preavis)
    {
        $this->preavis = $preavis;

        return $this;
    }

    /**
     * Get preavis
     *
     * @return string 
     */
    public function getPreavis()
    {
        return $this->preavis;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Location
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
}
