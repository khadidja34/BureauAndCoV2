<?php

namespace BureauAndCo\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *SalleFormation SurfaceExploite
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="num", message="Cette  Salle de formation existe déja, merci de vérifier le numéro saisi!.")
 */
class SalleFormation extends SurfaceExploite
{
    
}
