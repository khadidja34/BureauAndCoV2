<?php

namespace BureauAndCo\SiteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
/**
 *Bureau SurfaceExploite
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="num", message="Ce bureau existe déja, merci de vérifier le numéro saisi!.")
 */
class Bureau extends SurfaceExploite
{
    
}
