<?php

namespace BureauAndCo\SiteBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="BureauAndCo\SiteBundle\Repository\CategorieRepository")
 * @UniqueEntity(fields="nom", message="Cette catégorie existe déja")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;
     /**
       * @ORM\OneToMany(targetEntity="BureauAndCo\SiteBundle\Entity\Site", mappedBy="categorie", cascade={"remove"})
       * @ORM\JoinColumn(nullable=true)
      */
    private $sites;

    public function __construct()

    {
        $this->sites= new \Doctrine\Common\Collections\ArrayCollection();
       
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
     public function __toString(){
        return $this->getNom();
    }

    /**
     * Add sites
     *
     * @param \BureauAndCo\SiteBundle\Entity\Site $sites
     * @return Categorie
     */
    public function addSite(\BureauAndCo\SiteBundle\Entity\Site $sites)
    {
        $this->sites[] = $sites;

        return $this;
    }

    /**
     * Remove sites
     *
     * @param \BureauAndCo\SiteBundle\Entity\Site $sites
     */
    public function removeSite(\BureauAndCo\SiteBundle\Entity\Site $sites)
    {
        $this->sites->removeElement($sites);
    }

    /**
     * Get sites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSites()
    {
        return $this->sites;
    }
}
