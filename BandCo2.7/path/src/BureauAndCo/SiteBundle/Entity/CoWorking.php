<?php

namespace BureauAndCo\SiteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *CoWorking SurfaceExploite
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="num", message="Cet espace co_working ce est déja dans le système, merci de vérifier le numéro saisi!")
 */
class CoWorking extends SurfaceExploite
{
    
}
