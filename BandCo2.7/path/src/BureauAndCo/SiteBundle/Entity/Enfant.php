<?php

namespace BureauAndCo\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Enfant SurfaceExploite
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="num", message="Cet enfant est déja dans le système, merci de vérifier le numéro saisi!")
 */
class Enfant extends SurfaceExploite
{
     /**
       * @ORM\ManyToOne(targetEntity="BureauAndCo\SiteBundle\Entity\SurfaceExploite", inversedBy="enfants" )
       * @ORM\JoinColumn(nullable=false)
    */
   
    private $parent;

    /**
     * Set parent
     *
     * @param \BureauAndCo\SiteBundle\Entity\SurfaceExploite $parent
     * @return Enfant
     */
    public function setParent(\BureauAndCo\SiteBundle\Entity\SurfaceExploite $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \BureauAndCo\SiteBundle\Entity\SurfaceExploite 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
