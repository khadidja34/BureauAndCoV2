<?php

namespace BureauAndCo\SiteBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BureauAndCo\SiteBundle\Entity\SurfaceExploite;
use BureauAndCo\SiteBundle\Entity\SurfaceExploiteRepository;
use BureauAndCo\SiteBundle\Entity\Bureau;
use BureauAndCo\SiteBundle\Entity\Site;
use BureauAndCo\SiteBundle\Entity\Tarif;
use BureauAndCo\SiteBundle\Form\LocationType;
use BureauAndCo\SiteBundle\Form\NouvelleSurfaceType;
use BureauAndCo\SiteBundle\Form\LocataireType;
use BureauAndCo\SiteBundle\Form\EditerSurfaceType;
use BureauAndCo\SiteBundle\Entity\Location;
use BureauAndCo\SiteBundle\Entity\Locataire;
use BureauAndCo\SiteBundle\Form\DuplicateEnfantType;
use BureauAndCo\SiteBundle\Form\LocationDomiciliationType;
use BureauAndCo\SiteBundle\Entity\SalleReunion;
use BureauAndCo\SiteBundle\Entity\Enfant;
use BureauAndCo\SiteBundle\Entity\SalleFormation;
use BureauAndCo\SiteBundle\Entity\CoWorking;
use BureauAndCo\SiteBundle\Entity\Categorie;
use BureauAndCo\SiteBundle\Form\BureauType;
use BureauAndCo\SiteBundle\Form\EnfantType;
use BureauAndCo\SiteBundle\Form\SurfaceExploiteType;
use BureauAndCo\SiteBundle\Form\SalleReunionType;
use BureauAndCo\SiteBundle\Form\SalleFormationType;
use BureauAndCo\SiteBundle\Form\CoWorkingType;
use BureauAndCo\SiteBundle\Form\SiteType;
use BureauAndCo\SiteBundle\Form\CategorieType;
use BureauAndCo\SiteBundle\Form\TarifType;
use BureauAndCo\SiteBundle\Form\NewLocationType;
use BureauAndCo\UsersBundle\Entity\Client;
use BureauAndCo\UsersBundle\Entity\Entreprise;
use BureauAndCo\UsersBundle\Entity\Reservation;
use BureauAndCo\UsersBundle\Form\ReservationType;
use BureauAndCo\UsersBundle\Form\EntrepriseType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    /**
     * @Route("/nouveauSite", name="ns")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
    public function nouveauSiteAction(Request $request)
    {
      if($this->get('security.context')->isGranted('ROLE_ADMIN')) {
        $title="Nouveau site";
        $site = new Site();
        $form =$this->createForm(new SiteType(), $site);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
          if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($site);
            $em->flush();
            $request->getSession()
            ->getFlashBag()
            ->add('success', 'Le nouveau site a été bien ajouté');
            return $this->redirect($this->generateUrl('gestion'));
          }   
        }
        return $this->render('BureauAndCoSiteBundle:Default:nouveauSite.html.twig',array( 'form' => $form->createView()));
      }
      else{
        return new Response ("acces interdit à cette page!");
      }
    }
     /**
     * @Route("/nouvelleCat", name="nc")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
     public function nouvelleCatAction(Request $request)
     {
       if($this->get('security.context')->isGranted('ROLE_ADMIN')) {
         $title="Nouvelle catégorie";
         $cat = new Categorie();
         $form =$this->createForm(new CategorieType(), $cat);

         if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
          if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($cat);
            $em->flush();
            $request->getSession()
            ->getFlashBag()
            ->add('success', 'La nouvelle catégorie a été enregistrée avec succés');
            return $this->redirect($this->generateUrl('mesCat'));
          }   
        }
        return $this->render('BureauAndCoSiteBundle:Default:nouvelleCat.html.twig',array( 'form' => $form->createView()));
      }
      else{
        return new Response("Accés interdit à cette page!");
      }
    } 
     /**
      * @Route("/nouvelleLocation", name="nouvelleLocation")
      * @Security("has_role('ROLE_ASSISTANT')")
      */
      public function nouvelleLocationAction(Request $request)
      {
        $title="Nouvelle location";
        $message="";
        $em = $this->getDoctrine()->getManager();
        $location = new Locataire();

        $form=$this->createForm(new NewLocationType($em), $location);
       if ('POST' == $request->getMethod()) { // Si on a posté le formulaire

         $form->bind($this->get('request'));// On récupère les données du form
         if ($form->isValid()) {
          $repository = $this
          ->getDoctrine()
          ->getManager()
          ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
          $res = $repository->findOneBy(array('num' => $location->getSurface()));
          $res->setLibre("non");

          $em = $this->getDoctrine()->getManager();
            // $location->setDateFin($dateFin);
             // $location->setDateDebut($dateDebut);
          $em->persist($location);
          $em->flush();
          $request->getSession()
          ->getFlashBag()
          ->add('success', 'La nouvelle location a été enregistrée avec succés');
          return $this->redirect($this->generateUrl('gestionClient'));
           // } 
        }
      }  

      return $this->render('BureauAndCoSiteBundle:Default:locations.html.twig',array( 'message'=>$message,'title'=>$title,'form' => $form->createView()));
    }
      /**
     * @Route("/chargerSurface/{site}",  options = { "expose" = true }, name="chargerSurface",)
     * @Security("has_role('ROLE_ASSISTANT')")
     */
      public function chargerSurfaceAction(Request $request,$site)
      {
       if($request->isXmlHttpRequest()){
        $em = $this->getDoctrine()->getManager();
        $listSurface=$em->getRepository('BureauAndCoSiteBundle:SurfaceExploite')->findBy(array('site'=>$site,'libre'=>"oui",'a_enfant'=>"non"));
        if ($listSurface){
          $surfaces=array();
          foreach ($listSurface as $surface) {
            $surfaces[]=$surface->getNum();
          }
        }
        else{
          $surfaces=null;
        }
        $response=new JsonResponse();
        return $response->setData(array('surface'=>$surfaces));
      }
      else{
       throw new \Exception('accés interdit à cette ressource!');
     }
   }
       /**
      * @Route("/location/{id}", name="location")
      * @Security("has_role('ROLE_ASSISTANT')")
      */
       public function locationAction(Request $request, SurfaceExploite $surface)
       {
        $title="Contrat de location pour :".$surface->getNum();
        $message="";
        $location = new Locataire();
        $form=$this->createForm(new LocationType($em), $location);
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoSiteBundle:Locataire');
        $res = $repository->findBy(array('surface' => $surface->getNum(),'preavis'=>'non indiqué'));
        if(count($res)>0){
          $message=" Cette surface fait l'objet d'une location dont le préavis n'est pas indiqué ";
        }
        else{
         if($surface->getAenfant()=="oui"){
          return new Response(" Cette surface est désactivée!");
        }
        else{
          
          $em = $this->getDoctrine()->getManager();
          
           if ('POST' == $request->getMethod()) { // Si on a posté le formulaire

             $form->bind($this->get('request'));// On récupère les données du form
             if ($form->isValid()) {
              $surface->setLibre("non");
              $em = $this->getDoctrine()->getManager();
              $location->setSite($surface->getSite());
              $location->setSurface($surface);
              $em->persist($location);
              $em->flush();
              $request->getSession()
              ->getFlashBag()
              ->add('success', 'La nouvelle location a été enregistrée avec succés');
              return $this->redirect($this->generateUrl('gestionClient'));
              
            }
          }  
        }
      }
      
      return $this->render('BureauAndCoSiteBundle:Default:location.html.twig',array( 'message'=>$message,'title'=>$title,'form' => $form->createView()));
    }
    
      /**
      * @Route("/reservation/{id}", name="reservation")
      * @Security("has_role('ROLE_ASSISTANT')")
      */
      public function reservationAction(Request $request, SurfaceExploite $surface)
      {
        $title="Réservation pour :".$surface->getNum();
        $message="";
        $reservation = new Reservation();
        $form=$this->createForm(new ReservationType(), $reservation);
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoSiteBundle:Locataire');
        $res = $repository->findBy(array('surface' => $surface->getNum(),'preavis'=>'non indiqué'));
        if(count($res)>0){
          $message=" Cette surface fait l'objet d'une location dont le préavis n'est pas indiqué ";
        }
        else{
         if($surface->getAenfant()=="oui"){
          return new Response(" Cette surface est désactivée!");
        }
        else{
          
          $em = $this->getDoctrine()->getManager();
          
           if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
             $form->bind($this->get('request'));// On récupère les données du form
             if ($form->isValid()) {
               $surface->setLibre("réservé");
               $em = $this->getDoctrine()->getManager();
               $reservation->setSite($surface->getSite());
               $reservation->setSurface($surface);
               $em->persist($reservation);
               $em->flush();
               $request->getSession()
               ->getFlashBag()
               ->add('success', 'La réservation a été enregistrée avec succés');
               return $this->redirect($this->generateUrl('listReservation'));
             }
           }  
         }
       }
       
       return $this->render('BureauAndCoSiteBundle:Default:nouvelleReservation.html.twig',array( 'message'=>$message,'title'=>$title,'form' => $form->createView()));
     }

/**
 * @Route("/confirmerReservation/{id}", name="confirmerReservation")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

public function confirmerReservationAction(Request $request,Reservation $reservation)
{
//Je vérifie si le cleint est un prospect ou pas, si oui, il devient client interne
  $repository=$this
  ->getDoctrine()
  ->getManager()
  ->getRepository('BureauAndCoUsersBundle:Entreprise');
  $client=$repository->findBy(array('nom'=>$reservation->getNomClient()));
  if(count($client)==0){
    //il devient de type  interne
    $entreprise=new Entreprise();
    $entreprise->setNom($reservation->getNomClient()->getNom());
    $entreprise->setActivite($reservation->getNomClient()->getActivite());
    $form = $this->createForm(new EntrepriseType(), $entreprise);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->remove($reservation->getNomClient());
      if ($form->isValid()) {
        $em->flush();
        $em->persist($entreprise);
        $em->persist($entreprise->getRepresentant());
        $em->persist($entreprise->getPersonneAcontacter());
        $em->flush();
           //je crée la location à partir de la réservation
        $location=new Locataire();
        $location->setLocataire($entreprise);
        $location->setSite($reservation->getSite());
        $location->setSurface($reservation->getSurface());
        $location->setDateDebut($reservation->getDateDebut());
            //je met la surface à occupée
        $repository=$this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
        
        $surface=$repository->findOneBy(array('num'=>$reservation->getSurface()));
        $surface->setLibre("non");
        $em->persist($location);
        $em->flush();
         //Je supprime la réservation
        $em->remove($reservation);
        $em->flush(); 
         //redirection avec message de confirmation
        $request->getSession()
        ->getFlashBag()
        ->add('success', 'Un client prospect est devenu un client interne, félicitation!, le contrat est créé, la réservation est supprimée');
        return $this->redirect($this->generateUrl('gestionClient'));
      }   
    }
    return $this->render('BureauAndCoUsersBundle:Default:nouveauLocataire.html.twig',array( 'form' => $form->createView()));
  }
//si le client est un client interne
  else{
   $em=$this->getDoctrine()->getManager();
   $reservation->setConfirmation("oui");
   $em->flush(); 
// je crée le contrat
   $location=new Locataire();
   $location->setLocataire($reservation->getNomClient());
   $location->setSite($reservation->getSite());
   $location->setSurface($reservation->getSurface());
   $location->setDateDebut($reservation->getDateDebut());

   $repository=$this
   ->getDoctrine()
   ->getManager()
   ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
   
   $surface=$repository->findOneBy(array('num'=>$reservation->getSurface()));
   $surface->setLibre("non");
   $em->persist($location);
   $em->flush();
   //Je supprime la réservation
   $em->remove($reservation);
   $em->flush(); 
   $request->getSession()
   ->getFlashBag()
   ->add('success', 'La réservation a été transformée en contrat de location ');
   return $this->redirect($this->generateUrl('listLocation'));
 }

}

     /**
     * @Route("/nouvelleRecherche", name="recherche")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
     public function nouvelleRechercheAction(Request $request)
     {
       if($this->get('security.context')->isGranted('ROLE_ADMIN')) {
        $title="Nouvelle Recherche";
      }
      else{
        $title="Recherche dans le site :".$this->getUser()->getSite()->getNom();
      }
      $message="Aucun résultat n'a été trouvé!";
      $type="Tous type de surface";
      $form=$this->createFormBuilder()
      
      
      ->add('type', 'choice', array(
        'choices' => array(
          'Bureau' => 'Bureau',
          'SalleReunion' => 'SalleReunion',
          'SalleFormation' => 'SalleReunion',
          'CoWorking' => 'CoWorking',
          'Enfant'=>'Enfant(virtuel)'
          ),
        'required'    => false,
        'placeholder' => 'Quelle surface cherchez vous?',
        'empty_data'  => null
        ))
      ->add('libre', 'choice', array(
        'choices' => array(
          'oui' => 'Oui',
          'non' => 'Non',
          'réservé'=>'Réservé',
          ),
        'required'    => false,
        'placeholder' => 'disponibilité',
        'empty_data'  => null
        
        ))
      ->add('Rechercher', 'submit')
      ->getForm();
      if($this->get('security.context')->isGranted('ROLE_ADMIN')) {
        $form->add('site', 'entity', array('class' => 'BureauAndCoSiteBundle:Site',
          'choice_label' => 'nom'));
      }

      
      if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
       if ($form->isValid()) {
         if($this->get('security.context')->isGranted('ROLE_ADMIN')) 
         {
           $site=$form['site']->getData();
         }
         else{
          $site=$this->getUser()->getSite();
        }
        $type=$form['type']->getData();
        $libre=$form['libre']->getData();
        if($type=="SalleFormation"){
          $repository = $this
          ->getDoctrine()
          ->getManager()
          ->getRepository('BureauAndCoSiteBundle:SalleFormation');
          $data = $repository->findBy(array('site' => $site,'libre'=>$libre));
        }
        else if($type=="Bureau"){
         $repository = $this
         ->getDoctrine()
         ->getManager()
         ->getRepository('BureauAndCoSiteBundle:Bureau');
         $data = $repository->findBy(array('site' => $site,'libre'=>$libre));
       }
       else if($type=="SalleReunion"){
         $repository = $this
         ->getDoctrine()
         ->getManager()
         ->getRepository('BureauAndCoSiteBundle:SalleReunion');
         $data = $repository->findBy(array('site' => $site,'libre'=>$libre));
       }
       else if($type=="CoWorking"){
         $repository = $this
         ->getDoctrine()
         ->getManager()
         ->getRepository('BureauAndCoSiteBundle:CoWorking');
         $data = $repository->findBy(array('site' => $site,'libre'=>$libre));
       }
       else if($type=="Enfant"){
         $repository = $this
         ->getDoctrine()
         ->getManager()
         ->getRepository('BureauAndCoSiteBundle:Enfant');
         $data = $repository->findBy(array('site' => $site,'libre'=>$libre));
       }
       
       else{
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');                   
        $data = $repository->findAll();
      }
      return $this -> render("BureauAndCoSiteBundle:Default:resultatRecherche.html.twig", array('data'=>$data, 'type'=>$type,'message'=>$message,'title'=>$title));
    }   
  }

  return $this->render('BureauAndCoSiteBundle:Default:recherche.html.twig',array('form' => $form->createView(),'title'=>$title));
}
  /**
     * @Route("/resultatRecherche", name="resultatRecherche")
     * @Security("has_role('ROLE_ASSISTANT')")
  */
  public function resultatRechercheAction()
  {
    $message="Aucun résultat n'a été trouvé!";
    return $this->render('SiteBundle:Default:resultatRecherche.html.twig',array('message'=>$message));
  }
  /**
  *
  *@Route("/gestion", name="gestion")
  * @Security("has_role('ROLE_ASSISTANT')")
  */
  public function gestionAction()
  {  
   $listSites=array();
   $listeSites=array();
   $oui="oui";
   $em=$this->getDoctrine()->getManager();
   if($this->get('security.context')->isGranted('ROLE_ADMIN'))  {
    $listeSites = $em->getRepository('BureauAndCoSiteBundle:Site')->findAll();
    $paginator  = $this->get('knp_paginator');
    $listSites = $paginator->paginate($listeSites, $this->get('request')->query->get('page', 1), 3);
  }                        
  else {
    $user=$this->getUser();
    $listSites=$user->getSite();
  }
  return $this->render('BureauAndCoSiteBundle:Default:gestion.html.twig', array(
    'list'=>$listSites));
}
 /**
 * @Route("/details/{id}", name="details")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
 public function detailSiteAction(Site $site)
 { 
  $mysite =$site;
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   
   $rep = $this
   ->getDoctrine()
   ->getManager()
   ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');

   $messurfaces=$rep->findBy(array('site'=>$site));
 }

 else{
  if($mysite!=$this->getUser()->getSite()){
    return new Response ("Acces interdir à cette ressource!");
  }
  else{

   $mysite = $site;
   $rep = $this
   ->getDoctrine()
   ->getManager()
   ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
   $messurfaces=$rep->findBy(array('site'=>$site));
 
 }  
}
$paginator  = $this->get('knp_paginator');
$surfaces = $paginator->paginate($messurfaces, $this->get('request')->query->get('page', 1), 4);
$repository = $this
->getDoctrine()
->getManager()
->getRepository('BureauAndCoSiteBundle:Location');
$resultats=$repository->findBy(array('site'=>$site));

$reservation = $this
->getDoctrine()
->getManager()
->getRepository('BureauAndCoUsersBundle:Reservation');
$resultat=$reservation->findBy(array('site'=>$site));
$bureau=count($rep->findBy(array('site'=>$site,'type'=>"Bureau")));
$salleFormation=count($rep->findBy(array('site'=>$site,'type'=>"SalleFormation")));
$salleReunion=count($rep->findBy(array('site'=>$site,'type'=>"SalleReunion")));
$coworking=count($rep->findByType(array('site'=>$site,'type'=>"CoWorking")));

$total=$mysite->getNbBureau()+$mysite->getNbSalleFormation()+$mysite->getNbSalleReunion()+$mysite->getNbCoWorking();

$bar=(($bureau+$salleReunion+$salleFormation+$coworking)*100)/$total;
$bar=round($bar,0);
return $this->render('BureauAndCoSiteBundle:Default:detailSite.html.twig',array('mysite'=>$mysite,'resultat'=>$resultat,'resultats'=>$resultats,'surfaces'=>$surfaces, 'bar'=>$bar,'bureau'=>$bureau,'salleFormation'=>$salleFormation,'salleReunion'=>$salleReunion,'coworking'=>$coworking));
}
 /**
 * @Route("/supprimerSurface/{id}", name="supprimerSurface")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */
 public function SupprimerSurfaceAction(Request $request,SurfaceExploite $surface)
 { 
  $non="non";
  $test=(strstr($surface->getNum(),"Enfant"));
    // echo $test;
  
  if($test!=null){
    $parent=$surface->getParent();
    $enfants=$parent->getEnfants();
    if(count($enfants)<=1){
      $parent->setAEnfant($non);
    }
  }
  
  $em=$this->getDoctrine()->getManager();
  $em->remove($surface);
  $em->flush(); 
  $request->getSession()
  ->getFlashBag()
  ->add('success', 'Suppression effectuée avec succes');
  return $this->redirect($this->generateUrl('gestion'));
}
 /**
 * @Route("/enfant/{id}", name="enfant")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

 public function EnfantAction(Request $request,SurfaceExploite $surface)
 { 
  $title="Nouveau Enfant pour : ".$surface->getNum();
  $message="";
  $enfant= new Enfant();
  $enfant->setType("Enfant");
  $form =$this->createForm(new DuplicateEnfantType(), $enfant);
  if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
    $site=$surface->getSite();
    $enfant->setSite($site);
    $enfant->setParent($surface);

    $nb=$surface."/"."Enfant-".$enfant->getNum();
    $enfant->setNum($nb);
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:Enfant');
      $res = $repository->findOneBy(array('num' => $enfant->getNum()));
      if(count($res)!=0){
        $message=" Cet enfant existe déja dans le système, merci de vérifier le numéro saisi!";
      }
      else{
        $surface->setAEnfant("oui");
        $em->persist($enfant);
        $em->persist($enfant->getTarif());
        $em->flush();
        $request->getSession()
        ->getFlashBag()
        ->add('success', 'L\'enfant a été enregistré avec succés, Le parent a été désactivé!');
        return $this->redirect($this->generateUrl('gestion'));}
      }   
    }
    return $this->render('BureauAndCoSiteBundle:Default:DuplicateEnfant.html.twig',array('message'=>$message, 'title'=>$title, 'form' => $form->createView()));
  }

 /**
 * @Route("/supprimerSite/{id}", name="supprimerSite")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

 public function SupprimerSiteAction(Request $request, Site $site)
 { 
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
  $em=$this->getDoctrine()->getManager();
  $em->remove($site);
  $em->flush(); 
  $request->getSession()
  ->getFlashBag()
  ->add('success', 'Suppression effectuée avec succes');
  return $this->redirect($this->generateUrl('gestion')); 
}
else{
  return new Resposne("Acces interdit à cette ressource");
 }
}
 /**
 * @Route("/supprimerTache/{id}", name="supprimerTache")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */
 public function SupprimerTacheAction(Tache $tache)
 { 
  $em=$this->getDoctrine()->getManager();
  $em->remove($tache);
  $em->flush(); 
  $request->getSession()
  ->getFlashBag()
  ->add('success', 'Suppression effectuée avec succes');
  return $this->redirect($this->generateUrl('listeDesTaches'));
}
   /**
     * @Route("/editerSurface/{id}", name="editerSurface")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function editSurfaceAction(Request $request, SurfaceExploite $surface)
   {
    $title="Modification de la surface ".$surface->getNum();
    $message="";
    $formBureau =$this->createForm(new EditerSurfaceType(), $surface);
    if ($request->isMethod('POST') && $formBureau->handleRequest($request)->isValid()) {
     if ($formBureau->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
      $res = $repository->findOneBy(array('num' => $surface->getNum()));
      if(count($res)!=0){
        $message=" Cette surface existe déja dans le système, merci de vérifier le numéro saisi!";
      }
      $em->flush();
      $request->getSession()
      ->getFlashBag()
      ->add('success', 'Modification effectuée avec succès');
      return $this->redirect($this->generateUrl('gestion'));
    }
  }
  return $this->render('BureauAndCoSiteBundle:Default:editerSurface.html.twig',array('title'=>$title, 'message'=>$message,'form' => $formBureau->createView()));
}
/**
     * @Route("/editerLocation/{id}", name="editerLocation")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
public function editerLocationAction(Request $request, Location $location)
{
  $title="Modification de location";
  $message="";
  $em = $this->getDoctrine()->getManager();
  $form =$this->createForm(new LocationType($em), $location);
  if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
   if ($form->isValid()) {
    $em->flush();
    $request->getSession()
    ->getFlashBag()
    ->add('success', 'Modification effectuée avec succès');
    return $this->redirect($this->generateUrl('gestionClient'));
  }
}

return $this->render('BureauAndCoSiteBundle:Default:Location.html.twig',array( 'title'=>$title,'message'=>$message,'form' => $form->createView()));
}
/**
     * @Route("/editerReservation/{id}", name="editerReservation")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
public function editerReservationAction(Request $request, Reservation $reservation)
{
  $title="";
  $client=$reservation->getNomClient();
  $message="Modification d'une réservation pour le client : ".$client;
  $em = $this->getDoctrine()->getManager();
  $form =$this->createForm(new ReservationType($em), $reservation);
  if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
   if ($form->isValid()) {
    $em->flush();
    $request->getSession()
    ->getFlashBag()
    ->add('success', 'Modification effectuée avec succès');
    return $this->redirect($this->generateUrl('gestionClient'));
  }
}
return $this->render('BureauAndCoSiteBundle:Default:nouvelleReservation.html.twig',array( 'message'=>$message,'form' => $form->createView()));
}
   /**
     * @Route("/editerCat/{id}", name="editerCat")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function editCatAction(Request $request, Categorie $cat)
   {
 if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
    $form =$this->createForm(new CategorieType(), $cat);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($formCat->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
      ->getFlashBag()
      ->add('success', 'Modification effectuée avec succès');
      return $this->redirect($this->generateUrl('mesCat'));
    }
  }
  return $this->render('BureauAndCoSiteBundle:Default:nouvelleCat.html.twig',array( 'form' => $form->createView()));
}
else{
  return new Response("Acces interdit à cette ressource");
 }
}
   /**
     * @Route("/editerSite/{id}", name="editerSite")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   public function editSiteAction(Request $request, Site $site)
   {
    $form =$this->createForm(new SiteType(), $site);
    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
     if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();
      $request->getSession()
      ->getFlashBag()
      ->add('success', 'Modification effectuée avec succès');
      return $this->redirect($this->generateUrl('gestion'));
    }
  }
  return $this->render('BureauAndCoSiteBundle:Default:nouveauSite.html.twig',array( 'form' => $form->createView()));
}
   /**
     * @Route("/duplicate/{id}", name="duplicate")
     * @Security("has_role('ROLE_ASSISTANT')")
     */
   //Cette méthode va dupliquer une surface passée en argument.
   public function DuplicateAction(Request $request,SurfaceExploite $surface)
   {
     $title="Duplication";
     $message="";
     $table="SurfaceExploite";
     $type="SurfaceExploiteType";
     $nom=$surface->getNum();
     $em = $this->getDoctrine()->getManager();
     if(strstr($nom,"Enfant")){
       $table="Enfant";
       $duplicate = new Enfant();
       $duplicateType= new DuplicateEnfantType();

     }
     else if(strstr($nom,"Bureau")){
       $table="Bureau";
       $duplicate = new Bureau();
       $duplicateType= new BureauType();        

     }
     else if(strstr($nom,"SalleReunion")){
       $table="SalleReunion";
       $duplicate = new SalleReunion();
       $duplicateType= new SalleReunionType();

     }
     else if(strstr($nom,"SalleFormation")){
      $table="SalleFormation";
      $duplicate = new SalleFormation();
      $duplicateType= new SalleFormationType();
    }
    else if(strstr($nom,"CoWorking")){
      $table="CoWorking";
      $duplicate = new CoWorking();
      $duplicateType= new CoWorkingType();
    }
    $duplicate->setType($surface->getType());
    $duplicate->setNum("");
    $duplicate->setDescription($surface->getDescription());
    $duplicate->setEquipement($surface->getEquipement());
    $duplicate->setNbMax($surface->getNbMax());
    $duplicate->setEtage($surface->getEtage());
    $duplicate->setSuperficieReelle($surface->getSuperficieReelle());
    $duplicate->setSuperficieCommerciale($surface->getSuperficieCommerciale());
    $formBureau =$this->createForm(new $duplicateType(), $duplicate);
    if ($request->isMethod('POST') && $formBureau->handleRequest($request)->isValid()) {
      $sufixe=$duplicate->getSite();
      if($table=="Enfant"){
        $parent=$surface->getParent();
        $site=$parent->getSite();
        $duplicate->setSite($site);
        $duplicate->setParent($parent);
        $sufixe=$parent->getNum();
      }
      $nb=$sufixe."/".$table."-".$duplicate->getNum();
      $duplicate->setNum($nb);
      if ($formBureau->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('BureauAndCoSiteBundle:SurfaceExploite');
        $res = $repository->findOneBy(array('num' => $duplicate->getNum()));
        if(count($res)!=0){
          $message=" Cette surface existe déja dans le système, merci de vérifier le numéro saisi!";
        }
        else{
         $em->persist($duplicate);
         $em->persist($duplicate->getTarif());
         $em->flush();
         $request->getSession()
         ->getFlashBag()
         ->add('success', 'Duplication effectuée avec succès');
         return $this->redirect($this->generateUrl('gestion'));}
       }
     }
     if( $table=="Enfant"){
       return $this->render('BureauAndCoSiteBundle:Default:DuplicateEnfant.html.twig',array('title'=>$title,'message'=>$message, 'form' => $formBureau->createView()));
     }
     else{
       return $this->render('BureauAndCoSiteBundle:Default:nouveauBureau.html.twig',array('title'=>$title,'message'=>$message, 'formBureau' => $formBureau->createView()));
     }
   }
 /**
 * @Route("/confirmSupprimer/{id}", name="confirmSupprimer")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
 public function confirmSupprimerAction(SurfaceExploite $surface)
 { 
 
  $location=$this->getDoctrine()
  ->getRepository('BureauAndCoSiteBundle:Locataire')
  ->findBySurface($surface->getNum());

  $reservation=$this->getDoctrine()
  ->getRepository('BureauAndCoUsersBundle:Reservation')
  ->findBySurface($surface->getNum());

  return $this->render('BureauAndCoSiteBundle:Default:confirmSupprimer.html.twig',array( 'surface' => $surface,'location'=>$location,'reservation'=>$reservation));
}
 /**
 * @Route("/confirmSupprimerSite/{id}", name="confirmSupprimerSite")
 * @Security("has_role('ROLE_ASSISTANT')")
 * @return Response
 */
 public function confirmSupprimerSiteAction(Site $site)
 { 
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
  $oui="oui";
  $site = $this->getDoctrine()
  ->getRepository('BureauAndCoSiteBundle:Site')
  ->find($site);
  $location=$this->getDoctrine()
  ->getRepository('BureauAndCoSiteBundle:Location')
  ->findBySite($site);

  $locations=sizeof($location);

  $reservation=$this->getDoctrine()
  ->getRepository('BureauAndCoUsersBundle:Reservation')
  ->findBy(array('site'=>$site, 'confirmation'=>$oui));
  $reservations=sizeof($reservation);
  return $this->render('BureauAndCoSiteBundle:Default:confirmSupprimerSite.html.twig',array( 'site' => $site,'locations'=>$locations,'reservations'=>$reservations));
}
else{
  return new Response("Acces interdit à cette ressource");
 }
}
/**
 * @Route("/supprimerCat/{id}", name="supprimerCat")
 * @Security("has_role('ROLE_ASSISTANT')")
 * 
 * @return Response
 */

public function SupprimerCatAction(Request $request, Categorie $cat)
{ 
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
  $em=$this->getDoctrine()->getManager();
  $em->remove($cat);
  $em->flush(); 
  $request->getSession()
  ->getFlashBag()
  ->add('success', 'Suppression effectuée avec succes');
  return $this->redirect($this->generateUrl('mesCat'));
}
else{
  return new Response("Acces interdit à cette ressource");
 }
}

 /**
     * @Route("/nouvelleSurface", name="nouvelleSurface")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
 public function nouvelleSurfaceAction(Request $request)
 {
   $title="Nouveau espace de travail";
   $message="";
   $entity= new SurfaceExploite();
   // $entity="type"; 
   if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
     $form =$this->createForm(new NouvelleSurfaceType(), $entity);
   }
   else{
    $form =$this->createForm(new SurfaceExploiteType(), $entity);
    $form->remove('site');
  }
  if($this->get('request')->getMethod()=='POST'){
    $form->bind($this->get('request'));
    $type=$form['type']->getData();
    if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
      $site=$form['site']->getData();
    }
    else{
      $site=$this->getUser()->getSite();
    }
    $num=$form['num']->getData();
    $description=$form['description']->getData();
    $nbMax=$form['nbMax']->getData();
    $equipement=$form['equipement']->getData();
    $etage=$form['etage']->getData();
    $superficieRelle=$form['superficieReelle']->getData();
    $superficieCommerciale=$form['superficieCommerciale']->getData();
    $tarif=$form['tarif']->getData(); 
    if ($form->isValid()) {
     switch($type){
      case "Bureau":
      $entity = new Bureau();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:Bureau');
      $verif=$site->getNbBureau();
      break;
      //**ajouter une nouvelle salle de réunion
      case "SalleReunion":
      $entity = new SalleReunion();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:SalleReunion');
      $verif=$site->getNbSalleReunion();
      break;
      //**ajouter une nouvelle salle de formation
      case "SalleFormation":
      $entity = new SalleFormation();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:SalleFormation');
      $verif=$site->getNbSalleFormation();
      break;
       //**ajouter un nouveau espace co-working 
      case "CoWorking":
      $entity = new CoWorking();
      $repository = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('BureauAndCoSiteBundle:CoWorking');
      $verif=$site->getNbCoWorking();
      break;

    }
    if($type==null){
     $message=" Merci de choisir le type de la nouvelle surface";
   }
   else{
     if(count($repository->findBy(array('site'=>$site)))>=$verif){
      $message=" Ce site a déja toutes ses surfaces de type ".$type; 
    }
    else{

      $entity->setSite($site);
      $entity->setType($type);
      $entity->setNum($num);
      $entity->setDescription($description);
      $entity->setNbMax($nbMax);
      $entity->setEquipement($equipement);
      $entity->setEtage($etage);
      $entity->setSuperficieReelle($superficieRelle);
      $entity->setSuperficieCommerciale($superficieCommerciale);
      $entity->setTarif($tarif);
      $nb=$site."/".$type."-".$num;
      $entity->setNum($nb);
      $em = $this->getDoctrine()->getManager();
      
      $res = $repository->findOneBy(array('num' => $entity->getNum()));
      if(count($res)!=0){
        $message=" Cette surface est déja dans le système, merci de vérifier le numéro saisi!";
      }
      else{
        $em->persist($entity);
        $em->persist($entity->getTarif());
        $em->flush();
        $request->getSession()
        ->getFlashBag()
        ->add('success', 'La surface a été ajoutée avec succes à vos surfaces exploitées');
        return $this->redirect($this->generateUrl('gestion'));
      }
    }
  }
}
}
return $this->render('BureauAndCoSiteBundle:Default:nouvelleSurface.html.twig',array('title'=>$title, 'message'=>$message,'form'=>$form->createView()));
}

/**
     * @Route("/data/{table}/{dispo}", options={"expose"=true},  name="data")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
public function dataAction(Request $request, $table,$dispo)
{
  if($request->isXmlHttpRequest()){
   $em = $this->getDoctrine()->getManager();
   // $mysite=$em->getRepository('BureauAndCoSiteBundle:Site')->findById(array('id'=>$site));
   if($table=="Tous"){
    if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
      $data=$em->getRepository('BureauAndCoSiteBundle:SurfaceExploite')->findBy(array('libre' =>$dispo));
    }
    else{
     $data=$em->getRepository('BureauAndCoSiteBundle:SurfaceExploite')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
   }
 }

 if($table=="SalleFormation"){
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
    $data=$em->getRepository('BureauAndCoSiteBundle:SalleFormation')->findBy(array('libre' =>$dispo));
  }
  else{
    $data=$em->getRepository('BureauAndCoSiteBundle:SalleFormation')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
  }
}

if($table=="Bureau"){
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   $data=$em->getRepository('BureauAndCoSiteBundle:Bureau')->findBy(array('libre' =>$dispo));
 }
 else{
  $data=$em->getRepository('BureauAndCoSiteBundle:Bureau')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
}
}
if($table=="SalleReunion"){
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   $data=$em->getRepository('BureauAndCoSiteBundle:SalleReunion')->findBy(array('libre' =>$dispo));
 }
 else{
  $data=$em->getRepository('BureauAndCoSiteBundle:SalleReunion')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
}
}

if($table=="CoWorking"){
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   $data=$em->getRepository('BureauAndCoSiteBundle:CoWorking')->findBy(array('libre' =>$dispo));
 }
 else{
   $data=$em->getRepository('BureauAndCoSiteBundle:CoWorking')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
 }
}
if($table=="BoitePostale"){
 if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   $data=$em->getRepository('BureauAndCoSiteBundle:BoitePostale')->findBy(array('libre' =>$dispo));
 }
 else{
   $data=$em->getRepository('BureauAndCoSiteBundle:BoitePostale')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
 }
}
if($table=="Enfant"){
 if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
   $data=$em->getRepository('BureauAndCoSiteBundle:Enfant')->findBy(array('libre' =>$dispo));
 }
 else{
   $data=$em->getRepository('BureauAndCoSiteBundle:Enfant')->findBy(array('libre' =>$dispo,'site'=>$this->getUser()->getSite()));
 }
}
$info=array();
if($data){
 $info=array();
 foreach ($data as $num) {
  $info[]=$num=array('num'=>$num->getNum(),'equipement'=>$num->getEquipement(),'nbMax'=>$num->getNbMax());
}
}
else{
  $num="vide!";
}
$ObjetsJson = json_encode(array('resultat'=>$info));
return new response($ObjetsJson);
}
else{
  // return new Response("coucou");
 throw new \Exception(' Acces refusé!');
  }
}
 /**
     * @Route("/mesCategorie", name="mesCat")
     * @Security("has_role('ROLE_ASSISTANT')")
 */
 public function mesCategorieAction()
 {
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
    $repD=$this
    ->getDoctrine()
    ->getManager()
    ->getRepository('BureauAndCoSiteBundle:Categorie');

    $categories=$repD->findAll();
    return $this->render('BureauAndCoSiteBundle:Default:mesCategorie.html.twig',array('categorie'=>$categories));
  }
  else{
   return new Response( "Accés interdir à cette ressource");
 }
}
/**
 * @Route("/confirmSupprimerCat/{id}", name="confirmSupprimerCat")
 * @return Response
 */
public function confirmSupprimerCatAction(Categorie $cat)
{ 
  if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
  $cat = $this->getDoctrine()
  ->getRepository('BureauAndCoSiteBundle:Categorie')
  ->find($cat);
  return $this->render('BureauAndCoSiteBundle:Default:confirmSupprimerCat.html.twig',array('cat' => $cat));
}
else{
  return new Response("Acces interdit à cette ressource");
  }
}
   /**
  *
  *@Route("/listeLocations", name="listLocation")
  * @Security("has_role('ROLE_ASSISTANT')")
  */
   public function listeLocationsAction()
   { 
     if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
      $listeLocations =$this->getDoctrine()->getManager()->getRepository('BureauAndCoSiteBundle:Location')->findAll();
    }
    else {
     $listeLocations =$this->getDoctrine()->getManager()->getRepository('BureauAndCoSiteBundle:Location')->findBy(array('site'=>$this->getUser()->getSite()));
   }
   $paginator  = $this->get('knp_paginator');
   $listLocations = $paginator->paginate($listeLocations, $this->get('request')->query->get('page', 1), 4);
   return $this->render('BureauAndCoSiteBundle:Default:listeLocations.html.twig', array(
    'list'=>$listLocations));
 }

   /**
  *
  *@Route("/listReservation", name="listReservation")
  * @Security("has_role('ROLE_ASSISTANT')")
  */
   public function listeReservationsAction()
   { 
     if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
      $listeReservations =$this->getDoctrine()->getManager()->getRepository('BureauAndCoUsersBundle:Reservation')->findAll();
    }
    else{
      $listeReservations =$this->getDoctrine()->getManager()->getRepository('BureauAndCoUsersBundle:Reservation')->findBy(array('site'=>$this->getUser()->getSite()));
    }
    $paginator  = $this->get('knp_paginator');
    $listReservations = $paginator->paginate($listeReservations, $this->get('request')->query->get('page', 1), 4);
    return $this->render('BureauAndCoSiteBundle:Default:listeReservations.html.twig', array(
      'list'=>$listReservations));
  }
  /**
  *
  *@Route("/listeDesTaches", name="listeDesTaches")
  * @Security("has_role('ROLE_ASSISTANT')")
  */
  public function listeDesTachesAction()
  { 
    $title="Mes tâches";
    $listTaches=array();
    $rep=$this
    ->getDoctrine()
    ->getManager()
    ->getRepository('BureauAndCoUsersBundle:Tache');
    $taches = $rep->findBy(array('user'=>$this->getUser()));
    if(count($taches)>0){
      $paginator  = $this->get('knp_paginator');
      $listTaches = $paginator->paginate($taches, $this->get('request')->query->get('page', 1), 4);
    }
    else{
      $title="Vous n'avez aucune tâche";
    }
    return $this->render('BureauAndCoSiteBundle:Default:mesTaches.html.twig', array('title'=>$title,
      'tache'=>$listTaches));
  }
  
}
