<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoWorkingType extends AbstractType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\CoWorking'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_coworking';
    }
    public function getParent()
      {
        return new SurfaceExploiteType();
      }
}
