<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NouvelleSurfaceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('type', 'choice', array(
       'choices' => array(
        'Bureau' => 'Bureau',
        'SalleReunion' => 'Salle de réunion',
        'SalleFormation' => 'Salle de formation',
        'CoWorking' => 'CoWorking',
        'Enfant'=>'Enfant',
        'BoitePostale'=>'BoitePostale'
        ),
       'required'    =>false,
       'placeholder' => 'Choisissez le type ici',
       'empty_data'  => null
       ))
      ->add('num','text',array('required' => false))
      ->add('description','text',array('required' => false))
      ->add('nbMax','integer',array('required' => false))
      ->add('equipement','text',array('required' => false))
      ->add('superficieReelle','number',array('required' => false))
      ->add('superficieCommerciale','number', array('required' => false))
      ->add('etage','integer',array('required' => false))   
      ->add('tarif',new TarifType()) 
      ->add('site', 'entity', array(
    // query choices from this entity
        'class' => 'BureauAndCoSiteBundle:Site',
        'choice_label' => 'nom',

    // used to render a select box, check boxes or radios
    // 'multiple' => true,
    // 'expanded' => true,
        ))
      ->add('Valider','submit');

    }


    /**
     * @return string
     */
    public function getName()
    {
      return 'bureauandco_sitebundle_nouvelle_surface';
    }
    
  }
