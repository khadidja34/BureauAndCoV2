<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalleFormationType extends AbstractType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\SalleFormation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_salleformation';
    }
    public function getParent()
      {
        return new SurfaceExploiteType();
      }
}
