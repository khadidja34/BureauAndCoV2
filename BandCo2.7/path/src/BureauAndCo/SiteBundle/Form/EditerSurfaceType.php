<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditerSurfaceType extends AbstractType
{
 /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->remove('site');
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\SurfaceExploite'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_coworking';
    }
    public function getParent()
      {
        return new NouvelleSurfaceType();
      }
}
