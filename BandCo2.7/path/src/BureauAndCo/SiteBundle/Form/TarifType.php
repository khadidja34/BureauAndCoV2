<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TarifType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('devise','text', array('required' => false,'label'=>'La Devise' ))
            ->add('heure', 'text', array('required' => false,'label'=>'Heure HT' ))
            ->add('demijournee', 'text', array('required' => false,'label'=>'Demi-journée HT' ))
            ->add('journee','text', array('required' => false,'label'=>'La Journée HT' ))
            ->add('mois','text', array('required' => false,'label'=>'Le mois HT' ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\Tarif'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_tarif';
    }
}
