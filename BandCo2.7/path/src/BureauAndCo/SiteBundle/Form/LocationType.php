<?php

namespace BureauAndCo\SiteBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationType extends AbstractType
{
   /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
            ->remove('dateFin','datetime',array(
                                'placeholder' => array(
                                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                                'hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde', 
                                )))
            ->remove('preavis')
            ->add('locataire','entity',array('class' => 'BureauAndCoUsersBundle:Entreprise',
                                                'choice_label' => 'nom',))
            ->remove('site','entity', array(
    // query choices from this entity
                                      'class' => 'BureauAndCoSiteBundle:Site',
                                      'choice_label' => 'nom',
                                      'placeholder'=>'Choisissez un site ici',
                                       'attr'=>(array('class'=>'site'))))
            ->add('Valider', 'submit');
     }         
    /**
     * @param OptionsResolverInterface $resolver
     */
     public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'BureauAndCo\SiteBundle\Entity\Location'
            ));
        }

    /**
     * @return string
     */
    public function getName()
        {
            return 'bureauandco_sitebundle_location';
        }
}
