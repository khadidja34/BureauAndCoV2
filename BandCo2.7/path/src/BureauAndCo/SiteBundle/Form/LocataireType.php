<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class LocataireType extends AbstractType
{
       private $em;
     public function __construct($em)
     {
         $this->em=$em;
          // var_dump($em);
     }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
         ->remove('surface','choice',array('attr'=>array('class'=>'s')))
         ->add('Valider','submit');
        $variable=function(FormInterface $form, $site){

           $listSurface=$this->em->getRepository('BureauAndCoSiteBundle:SurfaceExploite')->findBy(array('site'=>$site,'libre'=>"oui",'a_enfant'=>"non"));

          if ($listSurface){

            $surfaces=array();

            foreach ($listSurface as $surface) {
            $surfaces[$surface->getNum()]=$surface->getNum();
            }
         }
         else{
              $surfaces="Aucun résultat n'a été trouvé";
           }

            $form->add('surface','choice',array('attr'=>array('class'=>'s'),
                                               'choices'=>$surfaces));

        };
        $builder->get('site')->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)use ($variable){
         $variable($event->getForm()->getParent(),$event->getForm()->getData());
        });
    }
       
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\Locataire'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_locataire';
    }
      public function getParent()
      {
        return new LocationType();
      }
}
