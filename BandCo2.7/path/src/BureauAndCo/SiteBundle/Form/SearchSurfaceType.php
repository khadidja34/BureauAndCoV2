<?php

namespace SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use SiteBundle\Entity\Site;
use SiteBundle\Form\SiteType;


class SearchSurface extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('site', 'entity', array(
    // query choices from this entity
                                      'class' => 'SiteBundle:Site',
                                      'choice_label' => 'nom',

    // used to render a select box, check boxes or radios
    // 'multiple' => true,
    // 'expanded' => true,
));
              ->add('type', 'choice', array(
                                                      'choices' => array(
                                                          'Bureau' => 'Bureau',
                                                          'SalleReunion' => 'Salle de réunion',
                                                          'SalleFormation' => 'Salle de formation',
                                                          'CoWorking' => 'Espace CoWorking',
                                                          'Parking' =>'Place dans le parking',
                                                          'BoitePostale'=>'Boîte postale'
                                                      ),
                                                      'required'    => false,
                                                      'placeholder' => 'type',
                                                      'empty_data'  => null
                                                  ))
             ->add('libre', 'choice', array(
                                                      'choices' => array(
                                                          'oui' => 'Oui',
                                                          'non' => 'Non'
                                                      ),
                                                      'required'    => false,
                                                      'placeholder' => 'Libre ou non?',
                                                      'empty_data'  => null
                                                  ))
              ->add('nbMax',     'integer' array('required' => false))
              ->add('Rechercher', 'submit');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBundle\Entity\SearchSurface'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sitebundle_searchsurface';
    }
     public function getParent()
      {
        return new SurfaceExploiteType();
      }
}
