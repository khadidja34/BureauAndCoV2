<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnfantType extends AbstractType
{
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->remove('site')
           ->add('parent','entity', array(
    // query choices from this entity
                                      'class' => 'BureauAndCoSiteBundle:SurfaceExploite',
                                      'choice_label' => 'num',
                                      'placeholder'=>'Choisissez un site ici',
                                       'attr'=>(array('class'=>'site'))))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\Enfant'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_enfant';
    }
     public function getParent()
      {
        return new SurfaceExploiteType();
      }
}
