<?php

namespace BureauAndCo\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom','text', array('required' => true))
      
            ->add('adresse','text',array('required' => true))
      
            ->add('ville','text',array('required' => true))
    
            ->add('pays','country',array('required' => true))
      
            ->add('tel','text',array('required' => true))
            ->add('superficie','number',array('required' => true))
            ->add('nb_bureau','integer',array('required' => true))
            ->add('nb_salle_reunion','integer',array('required' => true))
            ->add('nb_boite_postale','integer',array('required' => true))
            ->add('nb_salle_formation','integer',array('required' => true))
            ->add('nb_parking','integer',array('required' => true))
            ->add('nb_coworking','integer',array('required' => true))
            ->add('categorie','entity', array(
    // query choices from this entity
                                      'class' => 'BureauAndCoSiteBundle:Categorie',
                                      'choice_label' => 'nom',

    // used to render a select box, check boxes or radios
    // 'multiple' => true,
    // 'expanded' => true,
))
    ->add('Valider', 'submit');       
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BureauAndCo\SiteBundle\Entity\Site'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bureauandco_sitebundle_site';
    }
}
